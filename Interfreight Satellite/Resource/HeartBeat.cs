﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace Interfreight_Satellite
{
    static class HeartBeat
    {

        public static void RegisterHeartBeat(string custCode, string operation, ParameterInfo[] parameters)
        {
            try
            {
                SqlConnection sqlConn = new SqlConnection { ConnectionString = Globals.CTCconnString() };
                SqlCommand addHeartbeat = new SqlCommand
                {
                    CommandType = System.Data.CommandType.StoredProcedure,
                    CommandText = "AddHeartBeat",
                    Connection = sqlConn
                };

                Process currentProcess = Process.GetCurrentProcess();
                int pid = currentProcess.Id;
                SqlParameter pidParam = addHeartbeat.Parameters.Add("@PID", SqlDbType.Int);
                SqlParameter checkinParam = addHeartbeat.Parameters.Add("@CHECKIN", SqlDbType.DateTime);
                SqlParameter openParam = addHeartbeat.Parameters.Add("@OPENED", SqlDbType.DateTime);
                SqlParameter lastopParam = addHeartbeat.Parameters.Add("@LASTOP", SqlDbType.VarChar, 50);
                SqlParameter custidParam = addHeartbeat.Parameters.Add("@CUSTID", SqlDbType.UniqueIdentifier);
                SqlParameter startParam = addHeartbeat.Parameters.Add("@STARTING", SqlDbType.Bit);
                SqlParameter pathParam = addHeartbeat.Parameters.Add("@PATH", SqlDbType.VarChar, 200);
                SqlParameter appParam = addHeartbeat.Parameters.Add("@APPNAME", SqlDbType.VarChar, 50);
                SqlParameter paramParams = addHeartbeat.Parameters.Add("@PARAMS", SqlDbType.NVarChar, 100);
                appParam.Value = Assembly.GetExecutingAssembly().GetName().Name;
                pathParam.Value = Path.GetDirectoryName(Application.ExecutablePath);
                custidParam.Value = NodeResources.GetCustomer(custCode);
                pidParam.Value = pid;
                checkinParam.Value = DateTime.Now;

                if (operation == "Starting")
                {
                    startParam.Value = 1;
                    openParam.Value = DateTime.Now;
                }
                if (operation == "Stopping")
                {
                    startParam.Value = 1;
                    openParam.Value = null;
                    checkinParam.Value = null;
                    pidParam.Value = 0;

                }

                lastopParam.Value = operation;
                string pList = string.Empty;
                if (parameters != null)
                {
                    for (int i = 0; i < parameters.Length; i++)
                    {
                        pList += parameters[i].Name + ": " + parameters[i].ToString();
                    }
                }

                paramParams.Value = pList;
                if (sqlConn.State == System.Data.ConnectionState.Open)
                {
                    sqlConn.Close();
                }
                sqlConn.Open();
                addHeartbeat.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;

            }

        }
    }
}
