﻿using indice.Edi.Serialization;
using System;
using System.Collections.Generic;
namespace Interfreight_Satellite
{
    public class _315Gen
    {
        #region ISA an IEA
        [EdiValue("9(2)", Path = "ISA/0", Description = "ISA01 - Authorization Information Qualifier")]
        public int AuthorizationInformationQualifier { get; set; }

        [EdiValue("X(10)", Path = "ISA/1", Description = "ISA02 - Authorization Information")]
        public string AuthorizationInformation { get; set; }

        [EdiValue("9(2)", Path = "ISA/2", Description = "ISA03 - Security Information Qualifier")]
        public int Security_Information_Qualifier { get; set; }

        [EdiValue("X(10)", Path = "ISA/3", Description = "ISA04 - Security Information")]
        public string Security_Information { get; set; }

        [EdiValue("9(2)", Path = "ISA/4", Description = "ISA05 - Interchange ID Qualifier")]
        public string ID_Qualifier { get; set; }

        [EdiValue("X(15)", Path = "ISA/5", Description = "ISA06 - Interchange Sender ID")]
        public string Sender_ID { get; set; }

        [EdiValue("9(2)", Path = "ISA/6", Description = "ISA07 - Interchange ID Qualifier")]
        public string ID_Qualifier2 { get; set; }

        [EdiValue("X(15)", Path = "ISA/7", Description = "ISA08 - Interchange Receiver ID")]
        public string Receiver_ID { get; set; }

        [EdiValue("9(6)", Path = "ISA/8", Format = "yyMMdd", Description = "I09 - Interchange Date")]
        [EdiValue("9(4)", Path = "ISA/9", Format = "HHmm", Description = "I10 - Interchange Time")]
        public DateTime Date { get; set; }

        [EdiValue("X(1)", Path = "ISA/10", Description = "ISA11 - Interchange Control Standards ID")]
        public string Control_Standards_ID { get; set; }

        [EdiValue("9(5)", Path = "ISA/11", Description = "ISA12 - Interchange Control Version Num")]
        public int ControlVersion { get; set; }

        [EdiValue("9(9)", Path = "ISA/12", Description = "ISA13 - Interchange Control Number")]
        public int ControlNumber { get; set; }

        [EdiValue("9(1)", Path = "ISA/13", Description = "ISA14 - Acknowledgement Requested")]
        public bool? AcknowledgementRequested { get; set; }

        [EdiValue("X(1)", Path = "ISA/14", Description = "ISA15 - Usage Indicator")]
        public string Usage_Indicator { get; set; }

        [EdiValue("X(1)", Path = "ISA/15", Description = "ISA16 - Component Element Separator")]
        public char? Component_Element_Separator { get; set; }

        [EdiValue("9(1)", Path = "IEA/0", Description = "IEA01 - Num of Included Functional Grps")]
        public int GroupsCount { get; set; }

        [EdiValue("9(9)", Path = "IEA/1", Description = "IEA02 - Interchange Control Number")]
        public int TrailerControlNumber { get; set; }

        #endregion

        public List<FunctionalGroup> Groups { get; set; }

        [EdiGroup]
        public class FunctionalGroup
        {
            [EdiValue("X(2)", Path = "GS/0", Description = "GS01 - Functional Identifier Code")]
            public string FunctionalIdentifierCode { get; set; }

            [EdiValue("X(15)", Path = "GS/1", Description = "GS02 - Application Sender's Code")]
            public string ApplicationSenderCode { get; set; }

            [EdiValue("X(15)", Path = "GS/2", Description = "GS03 - Application Receiver's Code")]
            public string ApplicationReceiverCode { get; set; }

            [EdiValue("9(8)", Path = "GS/3", Format = "yyyyMMdd", Description = "GS04 - Date")]
            [EdiValue("9(4)", Path = "GS/4", Format = "HHmm", Description = "GS05 - Time")]
            public DateTime Date { get; set; }

            [EdiValue("9(9)", Path = "GS/5", Format = "HHmm", Description = "GS06 - Group Control Number")]
            public int GroupControlNumber { get; set; }

            [EdiValue("X(2)", Path = "GS/6", Format = "HHmm", Description = "GS07 Responsible Agency Code")]
            public string AgencyCode { get; set; }

            [EdiValue("X(2)", Path = "GS/7", Format = "HHmm", Description = "GS08 Version / Release / Industry Identifier Code")]
            public string Version { get; set; }


            public List<StatusMessage> StatusMessages { get; set; }


            [EdiValue("9(1)", Path = "GE/0", Description = "97 Number of Transaction Sets Included")]
            public int TransactionsCount { get; set; }

            [EdiValue("9(9)", Path = "GE/1", Description = "28 Group Control Number")]
            public int GroupTrailerControlNumber { get; set; }
        }

        [EdiMessage]
        public class StatusMessage
        {
            #region Header Trailer

            [EdiValue("X(3)", Path = "ST/0", Description = "ST01 - Transaction set ID code")]
            public string TransactionSetCode { get; set; }

            [EdiValue("X(9)", Path = "ST/1", Description = "ST02 - Transaction set control number")]
            public string TransactionSetControlNumber { get; set; }

            #endregion

            [EdiValue("X(3)", Path = "B4/1", Description = "B401 - Special Handling Code")]
            public string Special_Handling_Code { get; set; }

            [EdiValue("X(3)", Path = "B4/2", Description = "B402 - Inquiry Request Number(Not used)")]
            public string Inquiry_Request { get { return string.Empty; } }

            [EdiValue("X(2)", Path = "B4/3", Description = "B403 - Shipment Status Code")]
            public string Shipment_status_Code { get; set; }

            [EdiValue("9(8)", Path = "B4/4", Format = "yyyyMMdd", Description = "B404 - Status Date")]
            [EdiValue("9(4)", Path = "B4/5", Format = "HHmm", Description = "B405 - Status Time")]
            public DateTime Status_Date { get; set; }

            [EdiValue("X(5)", Path = "B4/6", Description = "B406 - Status Location")]
            public string Status_Location { get; set; }

            [EdiValue("X(4)", Path = "B4/7", Description = "B407 - Equipment Initial")]
            public string Equipment_Initial { get; set; }

            [EdiValue("X(10)", Path = "B4/8", Description = "B408 - Equipment Number")]
            public string Equipment_Number { get; set; }

            [EdiValue("X(2)", Path = "B4/9", Description = "B409 - Equipment Status Code")]
            public string Equipment_status_Code { get; set; }

            [EdiValue("9(4)", Path = "B4/10", Description = "B410 - Equipment Type")]
            public int Equipment_Type { get; set; }

            [EdiValue("X(30)", Path = "B4/11", Description = "B411 - Location Identifier")]
            public string Location_Identifier { get; set; }

            [EdiValue("X(2)", Path = "B4/12", Description = "B412 - Location Qualifier")]
            public string Location_Qualifier{ get; set; }

            [EdiValue("9(1)", Path = "B4/13", Description = "B413 - Check Digit")]
            public int Check_Digit { get; set; }

            public List<ReferenceIdentification> ReferenceIdentifications { get; set; }

            [EdiValue("9(8)", Path = "Q2/0", Description = "Q201 - Vessel Code")]
            public int Vessel_Code { get; set; }

            [EdiValue("X(10)", Path = "Q2/8", Description = "Q209 - Flight Voyage")]
            public string Flight_Voyage { get; set; }

            [EdiValue("X(1)", Path = "Q2/11", Description = "Q212 - Vessel Code Qualifier")]
            public string Vessel_Code_Qualifier { get; set; }

            [EdiValue("X(28)", Path = "Q2/12", Description = "Q213 - Vessel Name")]
            public string Vessel_Name { get; set; }



        }

        [EdiSegment,EdiSegmentGroup("N901", SequenceEnd ="Q2") ]
        public class ReferenceIdentification
        {
            [EdiValue("X(3)",Path = "N9/0", Description ="N901 - Reference Identification Qualifier")]
            public string Reference_Identification_Qualifier { get; set; }

            [EdiValue("X(30)", Path = "N9/1", Description ="N902 - Reference Identification")]
            public string Reference_Identification { get; set; }

            [EdiValue("X(45)", Path = "N9/2", Description = "N903 - Free-Form Description")]
            public string Freeform_Description { get; set; }

        }



        // This is just an example program to demonstrate how to generate an EDI X12 210 file using Framework EDI component in C#


    }

}
