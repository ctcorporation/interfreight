namespace Interfreight_Satellite.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class Processing_Errors
    {
        [Key]
        public Guid E_PK { get; set; }

        [StringLength(15)]
        public string E_SENDERID { get; set; }

        [StringLength(15)]
        public string E_RECIPIENTID { get; set; }

        public DateTime? E_PROCDATE { get; set; }

        public string E_FILENAME { get; set; }

        public string E_ERRORDESC { get; set; }

        [StringLength(10)]
        public string E_ERRORCODE { get; set; }

        public Guid? E_P { get; set; }

        [Required]
        [StringLength(1)]
        public string E_IGNORE { get; set; }
    }
}
