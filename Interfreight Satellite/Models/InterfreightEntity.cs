namespace Interfreight_Satellite.Models
{
    using System.Data.Entity;


    public partial class InterfreightEntity : DbContext
    {
        public InterfreightEntity()
            : base("name=InterfreightEntity")
        {
        }

        public InterfreightEntity(string connString)
            : base(connString)
        {

        }
        public virtual DbSet<Cargowise_Enums> Cargowise_Enums { get; set; }
        public virtual DbSet<CargowiseContext> CargowiseContexts { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Processing_Errors> Processing_Errors { get; set; }
        public virtual DbSet<Transaction_Log> Transaction_Logs { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }
        public virtual DbSet<Todo> ToDo { get; set; }
        public virtual DbSet<vw_ArchiveList> vw_ArchiveList { get; set; }
        public virtual DbSet<vw_TodoList> vw_TodoList { get; set; }
        public virtual DbSet<vw_Transaction> vw_Transaction { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cargowise_Enums>()
                .Property(e => e.CW_ENUMTYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Cargowise_Enums>()
                .Property(e => e.CW_ENUM)
                .IsFixedLength();

            modelBuilder.Entity<Cargowise_Enums>()
                .Property(e => e.CW_MAPVALUE)
                .IsFixedLength();

            modelBuilder.Entity<CargowiseContext>()
                .Property(e => e.CC_Context)
                .IsUnicode(false);

            modelBuilder.Entity<CargowiseContext>()
                .Property(e => e.CC_Description)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_IS_ACTIVE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_ON_HOLD)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_PATH)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_FTP_CLIENT)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_CODE)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_TRIAL)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_SHORTNAME)
                .IsFixedLength();

            modelBuilder.Entity<Processing_Errors>()
                .Property(e => e.E_SENDERID)
                .IsUnicode(false);

            modelBuilder.Entity<Processing_Errors>()
                .Property(e => e.E_RECIPIENTID)
                .IsUnicode(false);

            modelBuilder.Entity<Processing_Errors>()
                .Property(e => e.E_FILENAME)
                .IsUnicode(false);

            modelBuilder.Entity<Processing_Errors>()
                .Property(e => e.E_ERRORDESC)
                .IsUnicode(false);

            modelBuilder.Entity<Processing_Errors>()
                .Property(e => e.E_ERRORCODE)
                .IsFixedLength();

            modelBuilder.Entity<Processing_Errors>()
                .Property(e => e.E_IGNORE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Transaction_Log>()
                .Property(e => e.X_FILENAME)
                .IsUnicode(false);

            modelBuilder.Entity<Transaction_Log>()
                .Property(e => e.X_SUCCESS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Transaction_Log>()
                .Property(e => e.X_LASTRESULT)
                .IsUnicode(false);

            modelBuilder.Entity<Transaction>()
                .Property(e => e.T_FILENAME)
                .IsUnicode(false);

            modelBuilder.Entity<Transaction>()
                .Property(e => e.T_TRIAL)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Transaction>()
                .Property(e => e.T_INVOICED)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Transaction>()
                .Property(e => e.T_MSGTYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Transaction>()
                .Property(e => e.T_BILLTO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Transaction>()
                .Property(e => e.T_DIRECTION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Transaction>()
                .Property(e => e.T_CHARGEABLE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Transaction>()
                .Property(e => e.T_REF3)
                .IsUnicode(false);

            modelBuilder.Entity<Transaction>()
                .Property(e => e.T_ARCHIVE)
                .IsUnicode(false);

            modelBuilder.Entity<Transaction>()
                .Property(e => e.T_REF1TYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Transaction>()
                .Property(e => e.T_REF2TYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Transaction>()
                .Property(e => e.T_REF3TYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Todo>()
                .Property(e => e.L_FILENAME)
                .IsUnicode(false);

            modelBuilder.Entity<Todo>()
                .Property(e => e.L_LASTRESULT)
                .IsUnicode(false);

            modelBuilder.Entity<vw_ArchiveList>()
                .Property(e => e.T_FILENAME)
                .IsUnicode(false);

            modelBuilder.Entity<vw_ArchiveList>()
                .Property(e => e.T_REF1TYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_ArchiveList>()
                .Property(e => e.T_REF2TYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_ArchiveList>()
                .Property(e => e.T_REF3TYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_TodoList>()
                .Property(e => e.L_FILENAME)
                .IsUnicode(false);

            modelBuilder.Entity<vw_TodoList>()
                .Property(e => e.C_PATH)
                .IsUnicode(false);

            modelBuilder.Entity<vw_TodoList>()
                .Property(e => e.C_FTP_CLIENT)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_TodoList>()
                .Property(e => e.P_SERVER)
                .IsUnicode(false);

            modelBuilder.Entity<vw_TodoList>()
                .Property(e => e.P_USERNAME)
                .IsUnicode(false);

            modelBuilder.Entity<vw_TodoList>()
                .Property(e => e.P_PASSWORD)
                .IsUnicode(false);

            modelBuilder.Entity<vw_TodoList>()
                .Property(e => e.P_PORT)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_TodoList>()
                .Property(e => e.P_XSD)
                .IsUnicode(false);

            modelBuilder.Entity<vw_TodoList>()
                .Property(e => e.P_DIRECTION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_TodoList>()
                .Property(e => e.P_PATH)
                .IsUnicode(false);

            modelBuilder.Entity<vw_TodoList>()
                .Property(e => e.P_RECIPIENTID)
                .IsUnicode(false);

            modelBuilder.Entity<vw_TodoList>()
                .Property(e => e.C_CODE)
                .IsUnicode(false);

            modelBuilder.Entity<vw_TodoList>()
                .Property(e => e.P_DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<vw_TodoList>()
                .Property(e => e.P_REASONCODE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_TodoList>()
                .Property(e => e.P_DELIVERY)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_TodoList>()
                .Property(e => e.C_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<vw_TodoList>()
                .Property(e => e.P_BILLTO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_TodoList>()
                .Property(e => e.P_SENDERID)
                .IsUnicode(false);

            modelBuilder.Entity<vw_TodoList>()
                .Property(e => e.P_CHARGEABLE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_TodoList>()
                .Property(e => e.P_MSGTYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_TodoList>()
                .Property(e => e.P_MESSAGETYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_TodoList>()
                .Property(e => e.P_LIBNAME)
                .IsUnicode(false);

            modelBuilder.Entity<vw_TodoList>()
                .Property(e => e.P_DTS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_TodoList>()
                .Property(e => e.C_SHORTNAME)
                .IsFixedLength();

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.C_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.C_CODE)
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.C_IS_ACTIVE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.C_ON_HOLD)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.C_PATH)
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.C_FTP_CLIENT)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.T_FILENAME)
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.T_TRIAL)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.T_INVOICED)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.P_DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.P_REASONCODE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.P_RECIPIENTID)
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.T_CHARGEABLE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.T_MSGTYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.T_BILLTO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.P_MSGTYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.P_CHARGEABLE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.P_BILLTO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.T_DIRECTION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.P_DIRECTION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.B_BILLTO)
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.B_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.P_SENDERID)
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.P_DTS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.T_REF1TYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.T_REF2TYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.T_REF3TYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.P_GROUPCHARGES)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_Transaction>()
                .Property(e => e.C_SHORTNAME)
                .IsFixedLength();
        }
    }
}
