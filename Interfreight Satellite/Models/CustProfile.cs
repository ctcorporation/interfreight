﻿using System;

namespace Interfreight_Satellite
{
    public class CustProfileRecord
    {

        public Guid C_id
        {
            get;
            set;
        }
        public Guid P_id
        {
            get;
            set;
        }
        public String C_name
        {
            get;
            set;
        }
        public String C_is_active
        {
            get;
            set;
        }
        public String C_on_hold
        {
            get;
            set;
        }
        public String C_path
        {
            get;
            set;
        }
        public String C_ftp_client
        {
            get;
            set;
        }
        public String P_reasoncode
        {
            get;
            set;
        }
        public String P_EventCode
        {
            get;
            set;
        }
        public String P_server
        {
            get;
            set;
        }
        public String P_username
        {
            get;
            set;
        }
        public String P_path
        {
            get;
            set;
        }
        public String P_password
        {
            get;
            set;
        }

        public bool P_ssl
        {
            get; set;
        }
        public String P_delivery
        {
            get;
            set;
        }
        public String P_recipientid
        {
            get;
            set;
        }
        public String P_Senderid
        {
            get;
            set;
        }
        public String P_description
        {
            get;
            set;
        }
        public String P_port
        {
            get;
            set;
        }
        public String C_code
        {
            get;
            set;
        }
        public String P_Direction
        {
            get;
            set;
        }
        public Boolean P_DTS
        {
            get;
            set;
        }
        public String P_BillTo
        {
            get;
            set;
        }
        public String P_MsgType
        {
            get;
            set;
        }
        public string P_EmailAddress
        {
            get;
            set;
        }
        public string P_Subject
        {
            get;
            set;
        }

        public string P_Method
        {
            get;
            set;
        }
        public string P_Params
        {
            get;
            set;
        }

        public string P_NotifyEmail
        {
            get;
            set;
        }
    }
}
