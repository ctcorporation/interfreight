namespace Interfreight_Satellite.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vw_TodoList
    {
        [Key]
        [Column(Order = 0)]
        public Guid L_ID { get; set; }

        public string L_FILENAME { get; set; }

        public DateTime? L_DATE { get; set; }

        [StringLength(50)]
        public string C_PATH { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(1)]
        public string C_FTP_CLIENT { get; set; }

        public string P_SERVER { get; set; }

        public string P_USERNAME { get; set; }

        public string P_PASSWORD { get; set; }

        [StringLength(10)]
        public string P_PORT { get; set; }

        [StringLength(50)]
        public string P_XSD { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(1)]
        public string P_DIRECTION { get; set; }

        [StringLength(100)]
        public string P_PATH { get; set; }

        [StringLength(15)]
        public string P_RECIPIENTID { get; set; }

        [StringLength(15)]
        public string C_CODE { get; set; }

        public string P_DESCRIPTION { get; set; }

        [StringLength(3)]
        public string P_REASONCODE { get; set; }

        [StringLength(1)]
        public string P_DELIVERY { get; set; }

        [Key]
        [Column(Order = 3)]
        public Guid P_ID { get; set; }

        [StringLength(50)]
        public string C_NAME { get; set; }

        [StringLength(15)]
        public string P_BILLTO { get; set; }

        [StringLength(15)]
        public string P_SENDERID { get; set; }

        [StringLength(1)]
        public string P_CHARGEABLE { get; set; }

        [StringLength(20)]
        public string P_MSGTYPE { get; set; }

        [StringLength(3)]
        public string P_MESSAGETYPE { get; set; }

        [StringLength(50)]
        public string P_LIBNAME { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(1)]
        public string P_DTS { get; set; }

        [StringLength(10)]
        public string C_SHORTNAME { get; set; }
    }
}
