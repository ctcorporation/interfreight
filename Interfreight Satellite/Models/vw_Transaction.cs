namespace Interfreight_Satellite.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vw_Transaction
    {
        [Key]
        [Column(Order = 0)]
        public Guid C_ID { get; set; }

        [StringLength(50)]
        public string C_NAME { get; set; }

        [StringLength(15)]
        public string C_CODE { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(1)]
        public string C_IS_ACTIVE { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(1)]
        public string C_ON_HOLD { get; set; }

        [StringLength(50)]
        public string C_PATH { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(1)]
        public string C_FTP_CLIENT { get; set; }

        public DateTime? T_DATETIME { get; set; }

        [StringLength(100)]
        public string T_FILENAME { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(1)]
        public string T_TRIAL { get; set; }

        [Key]
        [Column(Order = 5)]
        [StringLength(1)]
        public string T_INVOICED { get; set; }

        public DateTime? T_INVOICEDATE { get; set; }

        public string P_DESCRIPTION { get; set; }

        [StringLength(3)]
        public string P_REASONCODE { get; set; }

        [StringLength(15)]
        public string P_RECIPIENTID { get; set; }

        [StringLength(1)]
        public string T_CHARGEABLE { get; set; }

        [StringLength(10)]
        public string T_MSGTYPE { get; set; }

        [StringLength(15)]
        public string T_BILLTO { get; set; }

        [StringLength(20)]
        public string P_MSGTYPE { get; set; }

        [StringLength(1)]
        public string P_CHARGEABLE { get; set; }

        [StringLength(15)]
        public string P_BILLTO { get; set; }

        public Guid? T_P { get; set; }

        [StringLength(1)]
        public string T_DIRECTION { get; set; }

        [StringLength(1)]
        public string P_DIRECTION { get; set; }

        public Guid? P_ID { get; set; }

        [Key]
        [Column(Order = 6)]
        public Guid T_ID { get; set; }

        [StringLength(15)]
        public string B_BILLTO { get; set; }

        [StringLength(50)]
        public string B_NAME { get; set; }

        [StringLength(15)]
        public string P_SENDERID { get; set; }

        [StringLength(1)]
        public string P_DTS { get; set; }

        [StringLength(50)]
        public string T_REF1 { get; set; }

        [StringLength(50)]
        public string T_REF2 { get; set; }

        [StringLength(50)]
        public string T_REF3 { get; set; }

        [StringLength(20)]
        public string T_ARCHIVE { get; set; }

        [StringLength(10)]
        public string T_REF1TYPE { get; set; }

        [StringLength(10)]
        public string T_REF2TYPE { get; set; }

        [StringLength(10)]
        public string T_REF3TYPE { get; set; }

        [StringLength(1)]
        public string P_GROUPCHARGES { get; set; }

        [StringLength(10)]
        public string C_SHORTNAME { get; set; }
    }
}
