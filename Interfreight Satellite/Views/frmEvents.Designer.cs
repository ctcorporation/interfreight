﻿namespace Interfreight_Satellite
{
    partial class frmEvents
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bbClose = new System.Windows.Forms.Button();
            this.dgEvents = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.edEvent = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.edDescr = new System.Windows.Forms.TextBox();
            this.cbShipment = new System.Windows.Forms.CheckBox();
            this.cbContainer = new System.Windows.Forms.CheckBox();
            this.bbAdd = new System.Windows.Forms.Button();
            this.cbOrigin = new System.Windows.Forms.CheckBox();
            this.cbDest = new System.Windows.Forms.CheckBox();
            this.EventCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Shipment = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Container = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.V_DESCRIPTION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.V_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgEvents)).BeginInit();
            this.SuspendLayout();
            // 
            // bbClose
            // 
            this.bbClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bbClose.Location = new System.Drawing.Point(760, 308);
            this.bbClose.Name = "bbClose";
            this.bbClose.Size = new System.Drawing.Size(75, 23);
            this.bbClose.TabIndex = 10;
            this.bbClose.Text = "&Close";
            this.bbClose.UseVisualStyleBackColor = true;
            this.bbClose.Click += new System.EventHandler(this.bbClose_Click);
            // 
            // dgEvents
            // 
            this.dgEvents.AllowUserToAddRows = false;
            this.dgEvents.AllowUserToDeleteRows = false;
            this.dgEvents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgEvents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgEvents.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EventCode,
            this.Column1,
            this.Column2,
            this.Shipment,
            this.Container,
            this.V_DESCRIPTION,
            this.V_ID});
            this.dgEvents.Location = new System.Drawing.Point(20, 75);
            this.dgEvents.Name = "dgEvents";
            this.dgEvents.ReadOnly = true;
            this.dgEvents.Size = new System.Drawing.Size(813, 214);
            this.dgEvents.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Event Code";
            // 
            // edEvent
            // 
            this.edEvent.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edEvent.Location = new System.Drawing.Point(88, 12);
            this.edEvent.MaxLength = 3;
            this.edEvent.Name = "edEvent";
            this.edEvent.Size = new System.Drawing.Size(70, 20);
            this.edEvent.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(184, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Description";
            // 
            // edDescr
            // 
            this.edDescr.Location = new System.Drawing.Point(250, 11);
            this.edDescr.Name = "edDescr";
            this.edDescr.Size = new System.Drawing.Size(462, 20);
            this.edDescr.TabIndex = 1;
            // 
            // cbShipment
            // 
            this.cbShipment.AutoSize = true;
            this.cbShipment.Checked = true;
            this.cbShipment.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShipment.Location = new System.Drawing.Point(21, 41);
            this.cbShipment.Name = "cbShipment";
            this.cbShipment.Size = new System.Drawing.Size(101, 17);
            this.cbShipment.TabIndex = 3;
            this.cbShipment.Text = "Shipment Event";
            this.cbShipment.UseVisualStyleBackColor = true;
            // 
            // cbContainer
            // 
            this.cbContainer.AutoSize = true;
            this.cbContainer.Location = new System.Drawing.Point(128, 41);
            this.cbContainer.Name = "cbContainer";
            this.cbContainer.Size = new System.Drawing.Size(102, 17);
            this.cbContainer.TabIndex = 4;
            this.cbContainer.Text = "Container Event";
            this.cbContainer.UseVisualStyleBackColor = true;
            // 
            // bbAdd
            // 
            this.bbAdd.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bbAdd.Location = new System.Drawing.Point(444, 37);
            this.bbAdd.Name = "bbAdd";
            this.bbAdd.Size = new System.Drawing.Size(75, 23);
            this.bbAdd.TabIndex = 7;
            this.bbAdd.Text = "&Add";
            this.bbAdd.UseVisualStyleBackColor = true;
            this.bbAdd.Click += new System.EventHandler(this.bbAdd_Click);
            // 
            // cbOrigin
            // 
            this.cbOrigin.AutoSize = true;
            this.cbOrigin.Location = new System.Drawing.Point(237, 41);
            this.cbOrigin.Name = "cbOrigin";
            this.cbOrigin.Size = new System.Drawing.Size(84, 17);
            this.cbOrigin.TabIndex = 5;
            this.cbOrigin.Text = "Origin Event";
            this.cbOrigin.UseVisualStyleBackColor = true;
            // 
            // cbDest
            // 
            this.cbDest.AutoSize = true;
            this.cbDest.Location = new System.Drawing.Point(328, 41);
            this.cbDest.Name = "cbDest";
            this.cbDest.Size = new System.Drawing.Size(110, 17);
            this.cbDest.TabIndex = 6;
            this.cbDest.Text = "Destination Event";
            this.cbDest.UseVisualStyleBackColor = true;
            // 
            // EventCode
            // 
            this.EventCode.DataPropertyName = "V_EVENT";
            this.EventCode.FillWeight = 70F;
            this.EventCode.HeaderText = "Event Code";
            this.EventCode.Name = "EventCode";
            this.EventCode.ReadOnly = true;
            this.EventCode.Width = 90;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "V_ORIGIN";
            this.Column1.HeaderText = "Origin Event";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column1.Width = 90;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "V_DEST";
            this.Column2.HeaderText = "Dest Event";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column2.Width = 90;
            // 
            // Shipment
            // 
            this.Shipment.DataPropertyName = "V_SHIPMENT";
            this.Shipment.FillWeight = 48.65519F;
            this.Shipment.HeaderText = "Shipment Event";
            this.Shipment.Name = "Shipment";
            this.Shipment.ReadOnly = true;
            this.Shipment.Width = 90;
            // 
            // Container
            // 
            this.Container.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Container.DataPropertyName = "V_CONTAINER";
            this.Container.FillWeight = 92.67934F;
            this.Container.HeaderText = "Container Event";
            this.Container.MinimumWidth = 70;
            this.Container.Name = "Container";
            this.Container.ReadOnly = true;
            this.Container.Width = 90;
            // 
            // V_DESCRIPTION
            // 
            this.V_DESCRIPTION.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.V_DESCRIPTION.DataPropertyName = "V_DESCRIPTION";
            this.V_DESCRIPTION.FillWeight = 59.7458F;
            this.V_DESCRIPTION.HeaderText = "Description";
            this.V_DESCRIPTION.MinimumWidth = 320;
            this.V_DESCRIPTION.Name = "V_DESCRIPTION";
            this.V_DESCRIPTION.ReadOnly = true;
            this.V_DESCRIPTION.Width = 320;
            // 
            // V_ID
            // 
            this.V_ID.DataPropertyName = "V_ID";
            this.V_ID.HeaderText = "V_ID";
            this.V_ID.Name = "V_ID";
            this.V_ID.ReadOnly = true;
            this.V_ID.Visible = false;
            // 
            // frmEvents
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(847, 343);
            this.Controls.Add(this.cbDest);
            this.Controls.Add(this.cbOrigin);
            this.Controls.Add(this.bbAdd);
            this.Controls.Add(this.cbContainer);
            this.Controls.Add(this.cbShipment);
            this.Controls.Add(this.edDescr);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.edEvent);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgEvents);
            this.Controls.Add(this.bbClose);
            this.Name = "frmEvents";
            this.Text = "EMOTrans Events";
            this.Load += new System.EventHandler(this.frmEvents_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgEvents)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bbClose;
        private System.Windows.Forms.DataGridView dgEvents;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox edEvent;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox edDescr;
        private System.Windows.Forms.CheckBox cbShipment;
        private System.Windows.Forms.CheckBox cbContainer;
        private System.Windows.Forms.Button bbAdd;
        private System.Windows.Forms.CheckBox cbOrigin;
        private System.Windows.Forms.CheckBox cbDest;
        private System.Windows.Forms.DataGridViewTextBoxColumn EventCode;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Shipment;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Container;
        private System.Windows.Forms.DataGridViewTextBoxColumn V_DESCRIPTION;
        private System.Windows.Forms.DataGridViewTextBoxColumn V_ID;
    }
}