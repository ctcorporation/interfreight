﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

namespace Interfreight_Satellite
{
    public partial class frmResendFile : Form
    {
        public SqlConnection sqlConn;
        public frmResendFile()
        {
            InitializeComponent();
        }

        private void bbClose_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void bbFind_Click(object sender, EventArgs e)
        {
            if (cbSearchArchive.Checked)
            {
                if (!string.IsNullOrEmpty(edArchive.Text) && File.Exists(edArchive.Text))
                {
                    FindInZip(edArchive.Text, edSupplierCode.Text, edFailedRef.Text);
                }
            }
            else
            {
                sqlConn = new SqlConnection();
                sqlConn.ConnectionString = Globals.connString();

                SqlDataAdapter da = new SqlDataAdapter("select * from vw_ArchiveList where (@REF2 IS NULL OR UPPER(T_REF2) Like '%'+@REF2+'%') and " +
                                                        "(@REF1 IS NULL OR UPPER(T_REF1) Like '%' + @REF1 + '%')  order by T_REF1,T_FILENAME", sqlConn);
                da.SelectCommand.Parameters.AddWithValue("@Ref1", edFailedRef.Text.Trim().ToUpper());
                da.SelectCommand.Parameters.AddWithValue("@Ref2", edSupplierCode.Text.Trim().ToUpper());

                DataSet ds = new DataSet();
                da.Fill(ds, "OrderView");
                dgResend.AutoGenerateColumns = false;
                dgResend.DataSource = ds;
                dgResend.DataMember = "OrderView";
                if (ds.Tables[0].Rows.Count == 0)
                {
                    MessageBox.Show("No records found.");
                }
            }
        }

        private void FindInZip(string archive, string supplierCode, string orderToFind)
        {
            using (var zip = ZipFile.Read(archive))
            {
                foreach (var e in zip.Entries)
                {
                    bool found = false;
                    DataRow dr = dsFind.Tables[0].NewRow();
                    using (var stream = e.OpenReader())
                    {
                        using (var reader = new StreamReader(stream))
                        {
                            var contents = reader.ReadToEnd();

                            if (!string.IsNullOrEmpty(supplierCode))
                            {
                                if (contents.Contains(supplierCode))
                                {
                                    found = true;
                                    dr["T_REF2"] = supplierCode;
                                }
                            }
                            if (!string.IsNullOrEmpty(orderToFind))
                            {
                                if (contents.Contains(orderToFind))
                                {
                                    found = true;
                                    dr["T_REF1"] = orderToFind;
                                }
                            }
                            if (found)
                            {
                                dr["T_FILENAME"] = e.FileName;
                                dr["T_REF2"] = supplierCode;
                                dr["T_DATETIME"] = e.LastModified;
                                dr["T_ARCHIVE"] = archive;
                                dsFind.Tables[0].Rows.Add(dr);
                            }
                        }
                    }
                }
            }
            dgResend.DataSource = dsFind.Tables[0];
        }

        private List<string> GetFileListFromZipFile(string archiveName)
        {
            List<string> result = new List<string>();
            using (ZipFile zip = ZipFile.Read(archiveName))
            {
                foreach (ZipEntry z in zip)
                {
                    result.Add(z.FileName);
                }
            }


            return result;
        }

        private string GetFileText(string filename)
        {
            string result = string.Empty;
            result = File.ReadAllText(filename);
            return result;
        }

        private void frmResendFile_Load(object sender, EventArgs e)
        {
            sqlConn = new SqlConnection();
            sqlConn.ConnectionString = Globals.connString();
        }

        private void btnResendFiles_Click(object sender, EventArgs e)
        {
            int iResendOrders = 0;
            int iMissingOrders = 0;
            foreach (DataGridViewRow row in dgResend.SelectedRows)
            {
                if (string.IsNullOrEmpty(row.Cells["T_ARCHIVE"].Value.ToString()))
                {
                    string strResend = Path.Combine(Globals.glFailPath, row.Cells["T_FILENAME"].Value.ToString().Trim());
                    if (File.Exists(Path.Combine(strResend)))
                    {

                        //TODO - Remove the Resend of previously completed file. It MUST be generated from the original file again. 
                        iResendOrders++;
                        NodeResources.MoveFile(strResend, Globals.glPickupPath);

                    }
                    else
                    {
                        iMissingOrders++;
                    }
                }
                else
                {
                    string strExtract = Globals.ExtractFile(Path.Combine(Globals.glArcLocation, row.Cells["T_ARCHIVE"].Value.ToString().Trim()), row.Cells["T_FILENAME"].Value.ToString().Trim(), Globals.glPickupPath);
                    if (strExtract.Contains("Warning:"))
                    {
                        MessageBox.Show(strExtract);
                    }
                    else
                    {
                        iResendOrders++;
                    }
                }


            }
            MessageBox.Show(iResendOrders + " Orders Resent and " + iMissingOrders + " that were either not found or have already been sent");
        }

        private void btnArc_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.InitialDirectory = Globals.glArcLocation;
            fd.ShowDialog();
            edArchive.Text = fd.FileName;

        }
    }

}
