﻿namespace Interfreight_Satellite
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.systemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customMappingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eventTypesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.diagnosticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataBaseTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emailTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fTPTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eAdapterTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resendFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslMain = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslSpacer = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslMode = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslCmbMode = new System.Windows.Forms.ToolStripDropDownButton();
            this.testingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tcMain = new System.Windows.Forms.TabControl();
            this.tabLog = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblFilesTx = new System.Windows.Forms.Label();
            this.lblCWTx = new System.Windows.Forms.Label();
            this.lblTotFilesTx = new System.Windows.Forms.Label();
            this.lblFilesRx = new System.Windows.Forms.Label();
            this.lblCwRx = new System.Windows.Forms.Label();
            this.lblTotFilesRx = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rtbLog = new System.Windows.Forms.RichTextBox();
            this.cmsLog = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.clearLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tbErrors = new System.Windows.Forms.TabPage();
            this.dgProcessingErrors = new System.Windows.Forms.DataGridView();
            this.E_P = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessingDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ErrorCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SenderID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecipientID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProfileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnTimer = new System.Windows.Forms.Button();
            this.bbClose = new System.Windows.Forms.Button();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tcMain.SuspendLayout();
            this.tabLog.SuspendLayout();
            this.panel1.SuspendLayout();
            this.cmsLog.SuspendLayout();
            this.tbErrors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProcessingErrors)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.systemToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(708, 27);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(41, 23);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // systemToolStripMenuItem
            // 
            this.systemToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.customMappingToolStripMenuItem,
            this.eventTypesToolStripMenuItem,
            this.profilesToolStripMenuItem,
            this.diagnosticsToolStripMenuItem,
            this.resendFilesToolStripMenuItem});
            this.systemToolStripMenuItem.Name = "systemToolStripMenuItem";
            this.systemToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.systemToolStripMenuItem.Size = new System.Drawing.Size(65, 23);
            this.systemToolStripMenuItem.Text = "&System";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(187, 24);
            this.settingsToolStripMenuItem.Text = "S&ettings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // customMappingToolStripMenuItem
            // 
            this.customMappingToolStripMenuItem.Name = "customMappingToolStripMenuItem";
            this.customMappingToolStripMenuItem.Size = new System.Drawing.Size(187, 24);
            this.customMappingToolStripMenuItem.Text = "Custom Mapping";
            this.customMappingToolStripMenuItem.Click += new System.EventHandler(this.customMappingToolStripMenuItem_Click);
            // 
            // eventTypesToolStripMenuItem
            // 
            this.eventTypesToolStripMenuItem.Name = "eventTypesToolStripMenuItem";
            this.eventTypesToolStripMenuItem.Size = new System.Drawing.Size(187, 24);
            this.eventTypesToolStripMenuItem.Text = "Event Types";
            this.eventTypesToolStripMenuItem.Click += new System.EventHandler(this.eventTypesToolStripMenuItem_Click);
            // 
            // profilesToolStripMenuItem
            // 
            this.profilesToolStripMenuItem.Name = "profilesToolStripMenuItem";
            this.profilesToolStripMenuItem.Size = new System.Drawing.Size(187, 24);
            this.profilesToolStripMenuItem.Text = "Profiles";
            this.profilesToolStripMenuItem.Click += new System.EventHandler(this.profilesToolStripMenuItem_Click);
            // 
            // diagnosticsToolStripMenuItem
            // 
            this.diagnosticsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataBaseTestToolStripMenuItem,
            this.emailTestToolStripMenuItem,
            this.fTPTestToolStripMenuItem,
            this.eAdapterTestToolStripMenuItem});
            this.diagnosticsToolStripMenuItem.Name = "diagnosticsToolStripMenuItem";
            this.diagnosticsToolStripMenuItem.Size = new System.Drawing.Size(187, 24);
            this.diagnosticsToolStripMenuItem.Text = "&Diagnostics";
            // 
            // dataBaseTestToolStripMenuItem
            // 
            this.dataBaseTestToolStripMenuItem.Name = "dataBaseTestToolStripMenuItem";
            this.dataBaseTestToolStripMenuItem.Size = new System.Drawing.Size(165, 24);
            this.dataBaseTestToolStripMenuItem.Text = "DataBase Test";
            // 
            // emailTestToolStripMenuItem
            // 
            this.emailTestToolStripMenuItem.Name = "emailTestToolStripMenuItem";
            this.emailTestToolStripMenuItem.Size = new System.Drawing.Size(165, 24);
            this.emailTestToolStripMenuItem.Text = "Email Test";
            // 
            // fTPTestToolStripMenuItem
            // 
            this.fTPTestToolStripMenuItem.Name = "fTPTestToolStripMenuItem";
            this.fTPTestToolStripMenuItem.Size = new System.Drawing.Size(165, 24);
            this.fTPTestToolStripMenuItem.Text = "FTP Test";
            // 
            // eAdapterTestToolStripMenuItem
            // 
            this.eAdapterTestToolStripMenuItem.Name = "eAdapterTestToolStripMenuItem";
            this.eAdapterTestToolStripMenuItem.Size = new System.Drawing.Size(165, 24);
            this.eAdapterTestToolStripMenuItem.Text = "eAdapter Test";
            this.eAdapterTestToolStripMenuItem.Click += new System.EventHandler(this.eAdapterTestToolStripMenuItem_Click);
            // 
            // resendFilesToolStripMenuItem
            // 
            this.resendFilesToolStripMenuItem.Name = "resendFilesToolStripMenuItem";
            this.resendFilesToolStripMenuItem.Size = new System.Drawing.Size(187, 24);
            this.resendFilesToolStripMenuItem.Text = "Resend Files";
            this.resendFilesToolStripMenuItem.Click += new System.EventHandler(this.resendFilesToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(49, 23);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(142, 24);
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslMain,
            this.tslSpacer,
            this.tslMode,
            this.tslCmbMode});
            this.statusStrip1.Location = new System.Drawing.Point(0, 441);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(708, 24);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslMain
            // 
            this.tslMain.AutoSize = false;
            this.tslMain.Name = "tslMain";
            this.tslMain.Size = new System.Drawing.Size(39, 19);
            this.tslMain.Text = "Status";
            this.tslMain.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tslSpacer
            // 
            this.tslSpacer.AutoSize = false;
            this.tslSpacer.Name = "tslSpacer";
            this.tslSpacer.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.tslSpacer.Size = new System.Drawing.Size(50, 19);
            // 
            // tslMode
            // 
            this.tslMode.Name = "tslMode";
            this.tslMode.Size = new System.Drawing.Size(59, 19);
            this.tslMode.Text = "tslMode";
            this.tslMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tslCmbMode
            // 
            this.tslCmbMode.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tslCmbMode.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tslCmbMode.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.testingToolStripMenuItem,
            this.productionToolStripMenuItem});
            this.tslCmbMode.Image = ((System.Drawing.Image)(resources.GetObject("tslCmbMode.Image")));
            this.tslCmbMode.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tslCmbMode.Margin = new System.Windows.Forms.Padding(0, 2, 10, 0);
            this.tslCmbMode.Name = "tslCmbMode";
            this.tslCmbMode.Size = new System.Drawing.Size(32, 22);
            this.tslCmbMode.Text = "toolStripDropDownButton1";
            // 
            // testingToolStripMenuItem
            // 
            this.testingToolStripMenuItem.CheckOnClick = true;
            this.testingToolStripMenuItem.Name = "testingToolStripMenuItem";
            this.testingToolStripMenuItem.Size = new System.Drawing.Size(147, 24);
            this.testingToolStripMenuItem.Text = "Testing";
            this.testingToolStripMenuItem.Click += new System.EventHandler(this.testingToolStripMenuItem_Click);
            // 
            // productionToolStripMenuItem
            // 
            this.productionToolStripMenuItem.CheckOnClick = true;
            this.productionToolStripMenuItem.Name = "productionToolStripMenuItem";
            this.productionToolStripMenuItem.Size = new System.Drawing.Size(147, 24);
            this.productionToolStripMenuItem.Text = "Production";
            this.productionToolStripMenuItem.Click += new System.EventHandler(this.productionToolStripMenuItem_Click);
            // 
            // tcMain
            // 
            this.tcMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcMain.Controls.Add(this.tabLog);
            this.tcMain.Controls.Add(this.tbErrors);
            this.tcMain.Location = new System.Drawing.Point(13, 28);
            this.tcMain.Name = "tcMain";
            this.tcMain.SelectedIndex = 0;
            this.tcMain.Size = new System.Drawing.Size(683, 373);
            this.tcMain.TabIndex = 3;
            // 
            // tabLog
            // 
            this.tabLog.Controls.Add(this.panel1);
            this.tabLog.Controls.Add(this.rtbLog);
            this.tabLog.Location = new System.Drawing.Point(4, 22);
            this.tabLog.Name = "tabLog";
            this.tabLog.Padding = new System.Windows.Forms.Padding(3);
            this.tabLog.Size = new System.Drawing.Size(675, 347);
            this.tabLog.TabIndex = 0;
            this.tabLog.Text = "Log";
            this.tabLog.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.lblFilesTx);
            this.panel1.Controls.Add(this.lblCWTx);
            this.panel1.Controls.Add(this.lblTotFilesTx);
            this.panel1.Controls.Add(this.lblFilesRx);
            this.panel1.Controls.Add(this.lblCwRx);
            this.panel1.Controls.Add(this.lblTotFilesRx);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(482, 9);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(188, 298);
            this.panel1.TabIndex = 7;
            // 
            // lblFilesTx
            // 
            this.lblFilesTx.AutoSize = true;
            this.lblFilesTx.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFilesTx.Location = new System.Drawing.Point(150, 178);
            this.lblFilesTx.Name = "lblFilesTx";
            this.lblFilesTx.Size = new System.Drawing.Size(16, 16);
            this.lblFilesTx.TabIndex = 12;
            this.lblFilesTx.Text = "0";
            // 
            // lblCWTx
            // 
            this.lblCWTx.AutoSize = true;
            this.lblCWTx.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCWTx.Location = new System.Drawing.Point(150, 150);
            this.lblCWTx.Name = "lblCWTx";
            this.lblCWTx.Size = new System.Drawing.Size(16, 16);
            this.lblCWTx.TabIndex = 11;
            this.lblCWTx.Text = "0";
            // 
            // lblTotFilesTx
            // 
            this.lblTotFilesTx.AutoSize = true;
            this.lblTotFilesTx.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotFilesTx.Location = new System.Drawing.Point(150, 122);
            this.lblTotFilesTx.Name = "lblTotFilesTx";
            this.lblTotFilesTx.Size = new System.Drawing.Size(16, 16);
            this.lblTotFilesTx.TabIndex = 10;
            this.lblTotFilesTx.Text = "0";
            // 
            // lblFilesRx
            // 
            this.lblFilesRx.AutoSize = true;
            this.lblFilesRx.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFilesRx.Location = new System.Drawing.Point(150, 94);
            this.lblFilesRx.Name = "lblFilesRx";
            this.lblFilesRx.Size = new System.Drawing.Size(16, 16);
            this.lblFilesRx.TabIndex = 9;
            this.lblFilesRx.Text = "0";
            // 
            // lblCwRx
            // 
            this.lblCwRx.AutoSize = true;
            this.lblCwRx.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCwRx.Location = new System.Drawing.Point(150, 66);
            this.lblCwRx.Name = "lblCwRx";
            this.lblCwRx.Size = new System.Drawing.Size(16, 16);
            this.lblCwRx.TabIndex = 8;
            this.lblCwRx.Text = "0";
            // 
            // lblTotFilesRx
            // 
            this.lblTotFilesRx.AutoSize = true;
            this.lblTotFilesRx.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotFilesRx.Location = new System.Drawing.Point(150, 38);
            this.lblTotFilesRx.Name = "lblTotFilesRx";
            this.lblTotFilesRx.Size = new System.Drawing.Size(16, 16);
            this.lblTotFilesRx.TabIndex = 7;
            this.lblTotFilesRx.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 178);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(133, 15);
            this.label8.TabIndex = 6;
            this.label8.Text = "Files to Non Cargowise";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 150);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(147, 15);
            this.label7.TabIndex = 5;
            this.label7.Text = "Files to Cargowise Clients";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 122);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 15);
            this.label6.TabIndex = 4;
            this.label6.Text = "Total Files Sent";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 94);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(143, 15);
            this.label5.TabIndex = 3;
            this.label5.Text = "Customer Files Received";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 66);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 15);
            this.label4.TabIndex = 2;
            this.label4.Text = "Cargowise Files";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 38);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 15);
            this.label3.TabIndex = 1;
            this.label3.Text = "Total Files Received";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 10);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(144, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Operation Summary";
            // 
            // rtbLog
            // 
            this.rtbLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbLog.ContextMenuStrip = this.cmsLog;
            this.rtbLog.Location = new System.Drawing.Point(7, 7);
            this.rtbLog.Name = "rtbLog";
            this.rtbLog.Size = new System.Drawing.Size(470, 300);
            this.rtbLog.TabIndex = 0;
            this.rtbLog.Text = "";
            // 
            // cmsLog
            // 
            this.cmsLog.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.cmsLog.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearLogToolStripMenuItem,
            this.exportLogToolStripMenuItem});
            this.cmsLog.Name = "cmsLog";
            this.cmsLog.Size = new System.Drawing.Size(145, 52);
            // 
            // clearLogToolStripMenuItem
            // 
            this.clearLogToolStripMenuItem.Name = "clearLogToolStripMenuItem";
            this.clearLogToolStripMenuItem.Size = new System.Drawing.Size(144, 24);
            this.clearLogToolStripMenuItem.Text = "Clear Log";
            this.clearLogToolStripMenuItem.Click += new System.EventHandler(this.clearLogToolStripMenuItem_Click);
            // 
            // exportLogToolStripMenuItem
            // 
            this.exportLogToolStripMenuItem.Name = "exportLogToolStripMenuItem";
            this.exportLogToolStripMenuItem.Size = new System.Drawing.Size(144, 24);
            this.exportLogToolStripMenuItem.Text = "Export Log";
            // 
            // tbErrors
            // 
            this.tbErrors.Controls.Add(this.dgProcessingErrors);
            this.tbErrors.Location = new System.Drawing.Point(4, 22);
            this.tbErrors.Name = "tbErrors";
            this.tbErrors.Padding = new System.Windows.Forms.Padding(3);
            this.tbErrors.Size = new System.Drawing.Size(675, 347);
            this.tbErrors.TabIndex = 1;
            this.tbErrors.Text = "Processing Errors";
            this.tbErrors.UseVisualStyleBackColor = true;
            // 
            // dgProcessingErrors
            // 
            this.dgProcessingErrors.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgProcessingErrors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgProcessingErrors.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.E_P,
            this.ProcessingDate,
            this.ErrorCode,
            this.Description,
            this.FileName,
            this.SenderID,
            this.RecipientID,
            this.ProfileName});
            this.dgProcessingErrors.Location = new System.Drawing.Point(16, 17);
            this.dgProcessingErrors.Name = "dgProcessingErrors";
            this.dgProcessingErrors.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgProcessingErrors.Size = new System.Drawing.Size(972, 324);
            this.dgProcessingErrors.TabIndex = 1;
            // 
            // E_P
            // 
            this.E_P.DataPropertyName = "E_PK";
            this.E_P.HeaderText = "E_P";
            this.E_P.Name = "E_P";
            this.E_P.Visible = false;
            // 
            // ProcessingDate
            // 
            this.ProcessingDate.DataPropertyName = "E_PROCDATE";
            dataGridViewCellStyle1.Format = "g";
            dataGridViewCellStyle1.NullValue = null;
            this.ProcessingDate.DefaultCellStyle = dataGridViewCellStyle1;
            this.ProcessingDate.HeaderText = "Time";
            this.ProcessingDate.Name = "ProcessingDate";
            this.ProcessingDate.Width = 200;
            // 
            // ErrorCode
            // 
            this.ErrorCode.DataPropertyName = "E_ERRORCODE";
            this.ErrorCode.HeaderText = "Error Code";
            this.ErrorCode.Name = "ErrorCode";
            this.ErrorCode.Width = 50;
            // 
            // Description
            // 
            this.Description.DataPropertyName = "E_ERRORDESC";
            this.Description.HeaderText = "Error Description";
            this.Description.Name = "Description";
            // 
            // FileName
            // 
            this.FileName.DataPropertyName = "E_FILENAME";
            this.FileName.HeaderText = "File Name";
            this.FileName.Name = "FileName";
            this.FileName.Width = 250;
            // 
            // SenderID
            // 
            this.SenderID.DataPropertyName = "E_SENDERID";
            this.SenderID.HeaderText = "Sender ";
            this.SenderID.Name = "SenderID";
            // 
            // RecipientID
            // 
            this.RecipientID.DataPropertyName = "E_RECIPIENTID";
            this.RecipientID.HeaderText = "Recipient";
            this.RecipientID.Name = "RecipientID";
            // 
            // ProfileName
            // 
            this.ProfileName.DataPropertyName = "P_DESCRIPTION";
            this.ProfileName.HeaderText = "Profile";
            this.ProfileName.Name = "ProfileName";
            this.ProfileName.Width = 250;
            // 
            // btnTimer
            // 
            this.btnTimer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTimer.BackColor = System.Drawing.Color.LightGreen;
            this.btnTimer.Image = ((System.Drawing.Image)(resources.GetObject("btnTimer.Image")));
            this.btnTimer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTimer.Location = new System.Drawing.Point(539, 407);
            this.btnTimer.Name = "btnTimer";
            this.btnTimer.Size = new System.Drawing.Size(75, 23);
            this.btnTimer.TabIndex = 6;
            this.btnTimer.Text = "&Start";
            this.btnTimer.UseVisualStyleBackColor = false;
            this.btnTimer.Click += new System.EventHandler(this.btnTimer_Click);
            // 
            // bbClose
            // 
            this.bbClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bbClose.Image = ((System.Drawing.Image)(resources.GetObject("bbClose.Image")));
            this.bbClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bbClose.Location = new System.Drawing.Point(621, 407);
            this.bbClose.Name = "bbClose";
            this.bbClose.Size = new System.Drawing.Size(75, 23);
            this.bbClose.TabIndex = 5;
            this.bbClose.Text = "&Close";
            this.bbClose.UseVisualStyleBackColor = true;
            this.bbClose.Click += new System.EventHandler(this.bbClose_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(277, 425);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(64, 15);
            this.linkLabel1.TabIndex = 7;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "linkLabel1";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(708, 465);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.btnTimer);
            this.Controls.Add(this.bbClose);
            this.Controls.Add(this.tcMain);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMain";
            this.Text = "Interfreight_Satellite";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tcMain.ResumeLayout(false);
            this.tabLog.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.cmsLog.ResumeLayout(false);
            this.tbErrors.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgProcessingErrors)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem systemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem diagnosticsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataBaseTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emailTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fTPTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eAdapterTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslMain;
        private System.Windows.Forms.TabControl tcMain;
        private System.Windows.Forms.TabPage tabLog;
        private System.Windows.Forms.RichTextBox rtbLog;
        private System.Windows.Forms.TabPage tbErrors;
        private System.Windows.Forms.ContextMenuStrip cmsLog;
        private System.Windows.Forms.ToolStripMenuItem clearLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profilesToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblFilesTx;
        private System.Windows.Forms.Label lblCWTx;
        private System.Windows.Forms.Label lblTotFilesTx;
        private System.Windows.Forms.Label lblFilesRx;
        private System.Windows.Forms.Label lblCwRx;
        private System.Windows.Forms.Label lblTotFilesRx;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgProcessingErrors;
        private System.Windows.Forms.DataGridViewTextBoxColumn E_P;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessingDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ErrorCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn SenderID;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecipientID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProfileName;
        private System.Windows.Forms.ToolStripMenuItem customMappingToolStripMenuItem;
        private System.Windows.Forms.Button btnTimer;
        private System.Windows.Forms.Button bbClose;
        private System.Windows.Forms.ToolStripDropDownButton tslCmbMode;
        private System.Windows.Forms.ToolStripMenuItem testingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productionToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel tslSpacer;
        private System.Windows.Forms.ToolStripStatusLabel tslMode;
        private System.Windows.Forms.ToolStripMenuItem eventTypesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resendFilesToolStripMenuItem;
        private System.Windows.Forms.LinkLabel linkLabel1;
    }
}

