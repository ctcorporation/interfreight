﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

namespace Interfreight_Satellite
{
    public partial class frmNewCustomer : Form
    {
        public SqlConnection sqlConn;
        public frmNewCustomer()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (sqlConn.State == ConnectionState.Open)
                sqlConn.Close();
            SqlCommand execAdd = new SqlCommand();
            execAdd.Connection = sqlConn;
            execAdd.CommandType = CommandType.StoredProcedure;
            if (btnSave.Text != "&Update")
            {
                execAdd.CommandText = "Add_Customer";
            }
            else
            {
                execAdd.CommandText = "Edit_Customer";
                SqlParameter C_ID = execAdd.Parameters.Add("@C_ID", SqlDbType.UniqueIdentifier);
                C_ID.Value = Globals.gl_CustId;
            }

            SqlParameter CName = execAdd.Parameters.Add("@C_NAME", SqlDbType.VarChar, 50);
            CName.Value = edCustomerName.Text;
            SqlParameter CCode = execAdd.Parameters.Add("@C_CODE", SqlDbType.VarChar, 15);
            CCode.Value = edCustCode.Text;
            SqlParameter CPath = execAdd.Parameters.Add("@C_PATH", SqlDbType.VarChar, 50);
            CPath.Value = edRootPath.Text;
            SqlParameter CShortName = execAdd.Parameters.Add("@C_SHORTNAME", SqlDbType.NChar, 10);
            CShortName.Value = edShortName.Text;
            SqlParameter CIsActive = execAdd.Parameters.Add("@C_IS_ACTIVE", SqlDbType.Char, 1);

            if (cbActive.Checked)
            {
                CIsActive.Value = "Y";
            }
            else
                CIsActive.Value = "N";
            SqlParameter COnHold = execAdd.Parameters.Add("@C_ON_HOLD", SqlDbType.Char, 1);
            if (cbHold.Checked)
            {
                COnHold.Value = "Y";
            }
            else
                COnHold.Value = "N";
            SqlParameter CFtpClient = execAdd.Parameters.Add("@C_FTP_CLIENT", SqlDbType.Char, 1);
            if (cbFtpClient.Checked)
            {
                CFtpClient.Value = "Y";
            }
            else
                CFtpClient.Value = "N";
            SqlParameter CTrial = execAdd.Parameters.Add("@C_TRIAL", SqlDbType.Char, 1);
            SqlParameter CTrialFrom = execAdd.Parameters.Add("@C_TRIALSTART", SqlDbType.DateTime);
            SqlParameter CTrialTo = execAdd.Parameters.Add("@C_TRIALEND", SqlDbType.DateTime);
            if (cbTrial.Checked)
            {
                CTrial.Value = 'Y';
                CTrialFrom.Value = DateTime.Parse(dtFrom.Text);
                CTrialTo.Value = DateTime.Parse(dtTo.Text);

            }
            else
            {
                CTrial.Value = 'N';
            }
            SqlParameter C_id = execAdd.Parameters.Add("@R_ID", SqlDbType.UniqueIdentifier);
            C_id.Direction = ParameterDirection.Output;
            SqlParameter R_Action = execAdd.Parameters.Add("@R_ACTION", SqlDbType.Char, 1);
            R_Action.Direction = ParameterDirection.Output;
            try
            {
                sqlConn.Open();
                execAdd.ExecuteNonQuery();
                Globals.gl_CustId = (Guid)C_id.Value;

                if (execAdd.Parameters["@R_ACTION"].Value.ToString() == "U")
                {

                    MessageBox.Show("Customer already exists. Customer updated", "Customer Updated", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Globals.CreateRootPath(edRootPath.Text);
                }
                else
                {
                    MessageBox.Show("Customer Added", "Customer Added", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    try
                    {
                        if (Directory.Exists(Path.Combine(edRootPath.Text, edCustCode.Text)))
                        {
                            DialogResult dr = MessageBox.Show("This Directory already Exists. Are you sure you wish to use ?", "Root Folder Exists", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (dr == DialogResult.Yes)
                            {
                                Globals.CreateRootPath(edRootPath.Text);
                            }
                        }
                        else
                        {
                            Globals.CreateRootPath(edRootPath.Text);
                        }
                        btnSave.Text = "&Update";

                    }
                    catch (Exception ex)
                    {

                    }
                }
                sqlConn.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Saving Record : " + ex.Message);
            }
            btnSave.Text = "&Save";

        }

        private void GetCustomer(string custCode)
        {
            SqlCommand sqlCust = new SqlCommand("Select C_ID, C_CODE, C_NAME, C_SHORTNAME, C_PATH, C_IS_ACTIVE, C_ON_HOLD, C_FTP_CLIENT  FROM CUSTOMER WHERE C_CODE = @C_CODE", sqlConn);
            sqlCust.Parameters.AddWithValue("@C_CODE", custCode);
            try
            {
                if (sqlConn.State == ConnectionState.Open)
                    sqlConn.Close();
                sqlConn.Open();
                SqlDataReader dr = sqlCust.ExecuteReader(CommandBehavior.CloseConnection);
                if (dr.HasRows)
                {
                    dr.Read();
                    edCustCode.Text = dr["C_CODE"].ToString();
                    edCustomerName.Text = dr["C_NAME"].ToString();
                    edShortName.Text = dr["C_SHORTNAME"].ToString();
                    edRootPath.Text = dr["C_PATH"].ToString();

                    if (dr["C_IS_ACTIVE"].ToString() == "Y")
                        cbActive.Checked = true;
                    else
                        cbActive.Checked = false;
                    if (dr["C_ON_HOLD"].ToString() == "Y")
                        cbHold.Checked = true;
                    else
                        cbHold.Checked = false;
                    if (dr["C_FTP_CLIENT"].ToString() == "Y")
                        cbFtpClient.Checked = true;
                    else
                        cbFtpClient.Checked = false;
                    Globals.gl_CustId = new Guid(dr["C_ID"].ToString());

                    sqlConn.Close();
                    btnSave.Text = "&Update";

                }
                else
                {
                    btnSave.Text = "&Save";
                    edRootPath.Text = Globals.glProfilePath;
                    edCustCode.Text = Globals.glCustCode;

                }

                // btnAdd.Text="E&dit";   
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was an error :" + ex.Message, "Error Locating Records", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void frmNewCustomer_Load(object sender, EventArgs e)
        {
            sqlConn = new SqlConnection { ConnectionString = Globals.connString() };
            GetCustomer(Globals.glCustCode);
        }
    }
}
