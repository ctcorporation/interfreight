﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace Interfreight_Satellite
{
    public partial class frmEnums : Form
    {
        SqlConnection sqlConn;
        public Guid cw_ID;
        public frmEnums()
        {
            InitializeComponent();
        }

        private void frmEnums_Load(object sender, EventArgs e)
        {
            sqlConn = new SqlConnection();
            sqlConn.ConnectionString = Globals.connString();
            LoadData();
        }


        private void LoadData()
        {
            SqlDataAdapter da = new SqlDataAdapter("SELECT CW_ID, CW_ENUMTYPE, CW_ENUM, CW_MAPVALUE FROM Cargowise_Enums ORDER BY CW_ENUMTYPE, CW_MAPVALUE ", sqlConn);
            DataSet ds = new DataSet();
            da.Fill(ds, "ENUMS");
            dgEnums.DataSource = ds;
            dgEnums.DataMember = "ENUMS";
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(edCargowiseValue.Text) && !string.IsNullOrEmpty(edCustomValue.Text) && !string.IsNullOrEmpty(cmbDataType.Text))
            {
                string msg = string.Empty;

                if (btnAdd.Text == "&Add")
                {
                    NodeResources.SetEnum(cmbDataType.Text, edCustomValue.Text, edCargowiseValue.Text);
                    msg = "Enum Data has been created";
                }
                else
                {
                    NodeResources.SetEnum(cmbDataType.Text, edCustomValue.Text, edCargowiseValue.Text, cw_ID);
                    msg = "Enum Data has been found and was updated";
                }
                MessageBox.Show(msg);
                cw_ID = Guid.Empty;
                LoadData();
            }
            btnAdd.Text = "&Add";
        }

        private void dgEnums_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > 0)
            {
                cw_ID = (Guid)dgEnums[0, e.RowIndex].Value;
                edCargowiseValue.Text = dgEnums["CWValue", e.RowIndex].Value.ToString().Trim();
                edCustomValue.Text = dgEnums["CW_MAPVALUE", e.RowIndex].Value.ToString().Trim();
                cmbDataType.Text = dgEnums["Type", e.RowIndex].Value.ToString().Trim();
                btnAdd.Text = "&Update";

            }

        }

        private void bbNew_Click(object sender, EventArgs e)
        {
            cmbDataType.Text = "";
            edCustomValue.Text = "";
            edCargowiseValue.Text = "";
            btnAdd.Text = "&Add";
            cw_ID = Guid.Empty;
            cmbDataType.Focus();
        }

        private void edDatatype_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (this.ActiveControl != null)
                {
                    this.SelectNextControl(this.ActiveControl, true, true, true, true);
                }
                e.Handled = true;
            }
        }

        private void bbFind_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(edFind.Text))
            {
                string searchValue = edFind.Text;
                DataGridViewRow row = dgEnums.Rows
                    .Cast<DataGridViewRow>()
                    .Where(r => r.Cells[3].Value.ToString().Trim().Contains(searchValue))
                    .FirstOrDefault();
                if (row != null)
                {
                    dgEnums.ClearSelection();
                    dgEnums.Rows[row.Index].Selected = true;
                    dgEnums.FirstDisplayedScrollingRowIndex = row.Index;
                }
                else
                {
                    MessageBox.Show("Item not found in Mappings");
                }

            }
        }

        private void dgEnums_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to delete the selected row?", "Remove Records", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                var id = e.Row.Cells[0].Value;
                SqlCommand delRow = new SqlCommand("DELETE FROM CARGOWISE_ENUMS where CW_ID = '" + id.ToString() + "'", sqlConn);
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                }
                sqlConn.Open();
                delRow.ExecuteNonQuery();
                LoadData();
            }




        }
    }
}
