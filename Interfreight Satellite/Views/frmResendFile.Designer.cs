﻿namespace Interfreight_Satellite
{
    partial class frmResendFile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bbClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.edFailedRef = new System.Windows.Forms.TextBox();
            this.bbFind = new System.Windows.Forms.Button();
            this.dgResend = new System.Windows.Forms.DataGridView();
            this.T_REF1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.T_REF2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.T_REF3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.T_DATETIME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.T_FILENAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.T_ARCHIVE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnResendFiles = new System.Windows.Forms.Button();
            this.edSupplierCode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.edArchive = new System.Windows.Forms.TextBox();
            this.btnArc = new System.Windows.Forms.Button();
            this.cbSearchArchive = new System.Windows.Forms.CheckBox();
            this.dsFind = new System.Data.DataSet();
            this.dtFiles = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgResend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsFind)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFiles)).BeginInit();
            this.SuspendLayout();
            // 
            // bbClose
            // 
            this.bbClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bbClose.Location = new System.Drawing.Point(808, 327);
            this.bbClose.Name = "bbClose";
            this.bbClose.Size = new System.Drawing.Size(75, 23);
            this.bbClose.TabIndex = 0;
            this.bbClose.Text = "&Close";
            this.bbClose.UseVisualStyleBackColor = true;
            this.bbClose.Click += new System.EventHandler(this.bbClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Enter Order or Reference Number";
            // 
            // edFailedRef
            // 
            this.edFailedRef.Location = new System.Drawing.Point(195, 17);
            this.edFailedRef.Name = "edFailedRef";
            this.edFailedRef.Size = new System.Drawing.Size(220, 20);
            this.edFailedRef.TabIndex = 2;
            // 
            // bbFind
            // 
            this.bbFind.Location = new System.Drawing.Point(432, 15);
            this.bbFind.Name = "bbFind";
            this.bbFind.Size = new System.Drawing.Size(75, 23);
            this.bbFind.TabIndex = 3;
            this.bbFind.Text = "&Find";
            this.bbFind.UseVisualStyleBackColor = true;
            this.bbFind.Click += new System.EventHandler(this.bbFind_Click);
            // 
            // dgResend
            // 
            this.dgResend.AllowUserToAddRows = false;
            this.dgResend.AllowUserToDeleteRows = false;
            this.dgResend.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgResend.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgResend.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.T_REF1,
            this.T_REF2,
            this.T_REF3,
            this.T_DATETIME,
            this.T_FILENAME,
            this.T_ARCHIVE,
            this.TID});
            this.dgResend.Location = new System.Drawing.Point(12, 89);
            this.dgResend.Name = "dgResend";
            this.dgResend.ReadOnly = true;
            this.dgResend.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgResend.Size = new System.Drawing.Size(871, 231);
            this.dgResend.TabIndex = 4;
            // 
            // T_REF1
            // 
            this.T_REF1.DataPropertyName = "T_REF1";
            this.T_REF1.HeaderText = "Reference";
            this.T_REF1.Name = "T_REF1";
            this.T_REF1.ReadOnly = true;
            this.T_REF1.Width = 150;
            // 
            // T_REF2
            // 
            this.T_REF2.DataPropertyName = "T_REF2";
            this.T_REF2.HeaderText = "Supplier Number";
            this.T_REF2.Name = "T_REF2";
            this.T_REF2.ReadOnly = true;
            // 
            // T_REF3
            // 
            this.T_REF3.DataPropertyName = "T_REF3";
            this.T_REF3.HeaderText = "Ref or Event";
            this.T_REF3.Name = "T_REF3";
            this.T_REF3.ReadOnly = true;
            // 
            // T_DATETIME
            // 
            this.T_DATETIME.DataPropertyName = "T_DATETIME";
            this.T_DATETIME.HeaderText = "Date Time";
            this.T_DATETIME.Name = "T_DATETIME";
            this.T_DATETIME.ReadOnly = true;
            this.T_DATETIME.Width = 150;
            // 
            // T_FILENAME
            // 
            this.T_FILENAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.T_FILENAME.DataPropertyName = "T_FILENAME";
            this.T_FILENAME.HeaderText = "Filename";
            this.T_FILENAME.Name = "T_FILENAME";
            this.T_FILENAME.ReadOnly = true;
            this.T_FILENAME.Width = 74;
            // 
            // T_ARCHIVE
            // 
            this.T_ARCHIVE.DataPropertyName = "T_ARCHIVE";
            this.T_ARCHIVE.HeaderText = "Archive Location";
            this.T_ARCHIVE.Name = "T_ARCHIVE";
            this.T_ARCHIVE.ReadOnly = true;
            this.T_ARCHIVE.Width = 170;
            // 
            // TID
            // 
            this.TID.DataPropertyName = "T_ID";
            this.TID.HeaderText = "ID";
            this.TID.Name = "TID";
            this.TID.ReadOnly = true;
            this.TID.Visible = false;
            // 
            // btnResendFiles
            // 
            this.btnResendFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResendFiles.Location = new System.Drawing.Point(808, 14);
            this.btnResendFiles.Name = "btnResendFiles";
            this.btnResendFiles.Size = new System.Drawing.Size(75, 23);
            this.btnResendFiles.TabIndex = 5;
            this.btnResendFiles.Text = "&Resend";
            this.btnResendFiles.UseVisualStyleBackColor = true;
            this.btnResendFiles.Click += new System.EventHandler(this.btnResendFiles_Click);
            // 
            // edSupplierCode
            // 
            this.edSupplierCode.Location = new System.Drawing.Point(195, 48);
            this.edSupplierCode.Name = "edSupplierCode";
            this.edSupplierCode.Size = new System.Drawing.Size(220, 20);
            this.edSupplierCode.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Enter Supplier Number";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(429, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Search Archive";
            // 
            // edArchive
            // 
            this.edArchive.Location = new System.Drawing.Point(515, 48);
            this.edArchive.Name = "edArchive";
            this.edArchive.Size = new System.Drawing.Size(345, 20);
            this.edArchive.TabIndex = 9;
            // 
            // btnArc
            // 
            this.btnArc.Location = new System.Drawing.Point(856, 46);
            this.btnArc.Name = "btnArc";
            this.btnArc.Size = new System.Drawing.Size(27, 23);
            this.btnArc.TabIndex = 10;
            this.btnArc.Text = "...";
            this.btnArc.UseVisualStyleBackColor = true;
            this.btnArc.Click += new System.EventHandler(this.btnArc_Click);
            // 
            // cbSearchArchive
            // 
            this.cbSearchArchive.AutoSize = true;
            this.cbSearchArchive.Location = new System.Drawing.Point(515, 17);
            this.cbSearchArchive.Name = "cbSearchArchive";
            this.cbSearchArchive.Size = new System.Drawing.Size(110, 17);
            this.cbSearchArchive.TabIndex = 11;
            this.cbSearchArchive.Text = "Search in Archive";
            this.cbSearchArchive.UseVisualStyleBackColor = true;
            // 
            // dsFind
            // 
            this.dsFind.DataSetName = "NewDataSet";
            this.dsFind.Tables.AddRange(new System.Data.DataTable[] {
            this.dtFiles});
            // 
            // dtFiles
            // 
            this.dtFiles.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6});
            this.dtFiles.TableName = "FoundFiles";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "T_REF1";
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "T_REF2";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "T_REF3";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "T_DATETIME";
            this.dataColumn4.DataType = typeof(System.DateTime);
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "T_FILENAME";
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "T_ARCHIVE";
            // 
            // frmResendFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(895, 362);
            this.Controls.Add(this.cbSearchArchive);
            this.Controls.Add(this.btnArc);
            this.Controls.Add(this.edArchive);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.edSupplierCode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnResendFiles);
            this.Controls.Add(this.dgResend);
            this.Controls.Add(this.bbFind);
            this.Controls.Add(this.edFailedRef);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bbClose);
            this.Name = "frmResendFile";
            this.Text = "Resend File";
            this.Load += new System.EventHandler(this.frmResendFile_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgResend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsFind)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFiles)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bbClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox edFailedRef;
        private System.Windows.Forms.Button bbFind;
        private System.Windows.Forms.DataGridView dgResend;
        private System.Windows.Forms.Button btnResendFiles;
        private System.Windows.Forms.TextBox edSupplierCode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn T_REF1;
        private System.Windows.Forms.DataGridViewTextBoxColumn T_REF2;
        private System.Windows.Forms.DataGridViewTextBoxColumn T_REF3;
        private System.Windows.Forms.DataGridViewTextBoxColumn T_DATETIME;
        private System.Windows.Forms.DataGridViewTextBoxColumn T_FILENAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn T_ARCHIVE;
        private System.Windows.Forms.DataGridViewTextBoxColumn TID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox edArchive;
        private System.Windows.Forms.Button btnArc;
        private System.Windows.Forms.CheckBox cbSearchArchive;
        private System.Data.DataSet dsFind;
        private System.Data.DataTable dtFiles;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
    }
}