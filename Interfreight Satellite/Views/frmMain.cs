﻿using CW;
using EdiEngine;
using EdiEngine.Common.Definitions;
using EdiEngine.Runtime;
using EdiEngine.Standards.X12_004010.Maps;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Timers;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using static Interfreight_Satellite.SatelliteErrors;
using CWShipment = CW.Shipment;
using SegmentDefinitions = EdiEngine.Standards.X12_004010.Segments;


namespace Interfreight_Satellite
{

    public partial class frmMain : Form
    {
        public SqlConnection sqlConn, sqlCTCConn;
        public int totfilesRx;
        public int totfilesTx;
        public int cwRx;
        public int cwTx;
        public int filesRx;
        public int filesTx;
        public System.Timers.Timer tmrMain;
        const string EMODTFormat = "yyyy-MM-ddTHH:mm:ss.ffzzz";
        public frmMain()
        {
            InitializeComponent();
            tmrMain = new System.Timers.Timer();
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnProcessExit);
            this.Text = string.Format("CTC Satellite - Interfreight {0}", Assembly.GetExecutingAssembly().GetName().Version.ToString());
        }

        private void OnProcessExit(object sender, EventArgs e)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, "Stopping", m.GetParameters());
        }

        private void tmrMain_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (btnTimer.Text == "&Stop")
            {
                tmrMain.Stop();
                GetFiles();
                tmrMain.Start();
            }
            else
            {
                tmrMain.Stop();
            }


        }

        private String getCustPath(String senderID)
        {
            SqlCommand sqlCustPath = new SqlCommand("SELECT @C_PATH=C_PATH from CUSTOMER where C_CODE = @C_CODE", sqlConn);
            if (sqlConn.State != ConnectionState.Closed)
            {
                sqlConn.Close();
            }
            sqlConn.Open();
            sqlCustPath.Parameters.AddWithValue("@C_CODE", senderID);
            SqlParameter CUSTPATH = sqlCustPath.Parameters.Add("@C_PATH", SqlDbType.VarChar, -1);
            CUSTPATH.Direction = ParameterDirection.Output;
            sqlCustPath.CommandType = CommandType.Text;
            sqlCustPath.ExecuteNonQuery();
            if (CUSTPATH.Value != DBNull.Value)
            {
                return CUSTPATH.Value.ToString();
            }
            else
            {
                return String.Empty;
            }
        }

        private Guid getCustID(String senderID)
        {
            SqlCommand sqlCustID = new SqlCommand("SELECT @C_ID=C_ID from CUSTOMER where C_CODE = @C_CODE", sqlConn);
            if (sqlConn.State != ConnectionState.Closed)
            {
                sqlConn.Close();
            }
            sqlConn.Open();
            sqlCustID.Parameters.AddWithValue("@C_CODE", senderID);
            SqlParameter CUSTID = sqlCustID.Parameters.Add("@C_ID", SqlDbType.UniqueIdentifier);
            CUSTID.Direction = ParameterDirection.Output;
            sqlCustID.CommandType = CommandType.Text;
            sqlCustID.ExecuteNonQuery();
            if (CUSTID.Value != DBNull.Value)
            {
                return (Guid)CUSTID.Value;
            }
            else
            {
                return Guid.Empty;
            }
        }

        private void btnTimer_Click(object sender, EventArgs e)
        {
            if (((Button)sender).Text == "&Start")
            {
                ((Button)sender).Text = "&Stop";
                ((Button)sender).BackColor = Color.LightCoral;
                tslMain.Text = "Timed Processes Started";
                GetFiles();
                //  PrintJobs("");
                tmrMain.Start();

            }
            else
            {
                ((Button)sender).Text = "&Start";
                ((Button)sender).BackColor = Color.LightGreen;
                tslMain.Text = "Timed Processes Stopped";
                tmrMain.Stop();
            }
        }

        public ProcessResult Create315(string xmlFile, CustProfileRecord cust)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            ProcessResult result = new ProcessResult();
            M_315 map315 = new M_315();
            EdiTrans ediTrans = new EdiTrans(map315);
            string poNumber = string.Empty;
            string ediPath = string.Empty;
            CW.UniversalInterchange cwFile = new CW.UniversalInterchange();
            using (FileStream fStream = new FileStream(xmlFile, FileMode.Open))
            {
                XmlSerializer cwConvert = new XmlSerializer(typeof(CW.UniversalInterchange));
                cwFile = (CW.UniversalInterchange)cwConvert.Deserialize(fStream);
                fStream.Close();
            }
            CW.Shipment shipment = new CWShipment();
            shipment = cwFile.Body.BodyField.Shipment;
            //CW.CommercialInfo commercialInfo = new CW.CommercialInfo();
            //shipment.DataContext.ActionPurpose = new CodeDescriptionPair();
            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Now Processing 315 Message Files.");
            if (!string.IsNullOrEmpty(shipment.DataContext.ActionPurpose.Code))
            {
                bool seaFreight = false;
                if (shipment.TransportMode.Code == "SEA")
                {
                    seaFreight = true;
                }
                else
                {
                    seaFreight = false;
                }
                if (seaFreight)
                {

                    if (shipment.ContainerCollection.Container != null)
                    {

                        foreach (Container container in shipment.ContainerCollection.Container)
                        {
                            EdiSimpleDataElement b4mode = null;
                            EdiSimpleDataElement b4StatCode = null;
                            EdiSimpleDataElement b4ContInitials = null;
                            EdiSimpleDataElement b4ContNumber = null;
                            EdiSimpleDataElement b4CheckDigit = null;

                            bool held = false;
                            int checkDigit;
                            var sDef = (MapSegment)map315.Content.First(s => s.Name == "B4");
                            var seg = new EdiSegment(sDef);
                            if (shipment.TransportMode.Code == "SEA")
                            {
                                b4mode = new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "OOL");
                                seaFreight = true;
                            }
                            else
                            {
                                b4mode = new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "ART");
                                seaFreight = false;
                            }
                            switch (shipment.DataContext.EventType.Code)
                            {
                                case "CLR":
                                    b4StatCode = new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], "CT");
                                    held = false;
                                    break;
                                case "CIP":
                                    b4StatCode = new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], "C1");
                                    held = true;
                                    break;
                                case "CCC":
                                    b4StatCode = new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], "GO");
                                    held = true;
                                    break;
                                case "CES":
                                    b4StatCode = new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], "C1");
                                    held = true;
                                    break;
                                default:
                                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "Un-Mapped event code found :" + shipment.DataContext.EventType.Code + " Skipping file.", Color.Blue);
                                    NodeResources.MoveFile(xmlFile, Globals.glFailPath);
                                    result.Processed = true;
                                    result.FileName = Path.GetFileName(xmlFile);
                                    result.FolderLoc = Globals.glFailPath;
                                    return result;
                                    break;
                            }
                            seg = new EdiSegment(sDef);
                            string containerNo = container.ContainerNumber;
                            if (!string.IsNullOrEmpty(container.ContainerNumber))
                            {
                                b4ContInitials = new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[6], container.ContainerNumber.Substring(0, 4));
                                b4ContNumber = new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[7], container.ContainerNumber.Substring(4, container.ContainerNumber.Length - 4));
                                if (int.TryParse(container.ContainerNumber.Substring(container.ContainerNumber.Length - 1, 1), out checkDigit))
                                {
                                    b4CheckDigit = new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[12], checkDigit.ToString());
                                }
                                seg.Content.AddRange(new[] {
                                b4mode,  //B401
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], String.Empty), //B402 Not used
                                b4StatCode, //B403 
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[3], string.Format("{0:yyyyMMdd}",DateTime.Now)), // B404 Date
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[4], string.Format("{0:hhmm}",DateTime.Now)), // B405 Time
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[5], shipment.PortOfFirstArrival.Code), // B406 Location
                                b4ContInitials, // B407 Container INIT
                                b4ContNumber, // B408 Container Numbers
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[8], "L"), //B409                                
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[9], string.Empty),//B410                                
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[10], shipment.PortOfFirstArrival.Name.Truncate(30)), //B411
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[11], "UN"),//B412
                                b4CheckDigit // B413
                                });
                                ediTrans.Content.Add(seg);
                                sDef = (MapSegment)map315.Content.First(s => s.Name == "N9");
                                seg = new EdiSegment(sDef);
                                if (shipment.EntryHeaderCollection != null)
                                {
                                    if (shipment.EntryHeaderCollection[0].EntryNumberCollection != null)
                                    {
                                        if (!string.IsNullOrEmpty(shipment.EntryHeaderCollection[0].EntryNumberCollection[0].Number))
                                        {
                                            seg.Content.AddRange(new[] {
                                        new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "6B"), //Customs Entry No
                                        new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], shipment.EntryHeaderCollection[0].EntryNumberCollection[0].Number),//Entry Number
                                        new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], "CUSTOMS-ENTRY NO") });
                                            ediTrans.Content.Add(seg);
                                        }
                                    }
                                }



                                if (!string.IsNullOrEmpty(shipment.AdditionalBillCollection[0].BillNumber) && shipment.AdditionalBillCollection[0].BillType.Code == "MWB")
                                {
                                    seg = new EdiSegment(sDef);
                                    seg.Content.AddRange(new[]{
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "MB"), //Master/ OBL
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], shipment.AdditionalBillCollection[0].BillNumber),//MBL/OBL NO
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], "OCEAN BILL") });
                                    ediTrans.Content.Add(seg);
                                }
                                seg = new EdiSegment(sDef);
                                if (seaFreight)
                                {
                                    seg.Content.AddRange(new[]{
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "EQ"), //Equipment No
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], container.ContainerNumber),//Container No
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], "EQUIPMENT NO") });
                                    ediTrans.Content.Add(seg);
                                }
                                if (!string.IsNullOrEmpty(shipment.WayBillType.Code))
                                {
                                    seg = new EdiSegment(sDef);
                                    if (shipment.WayBillType.Code == "HWB")
                                    {
                                        string hwb = (shipment.TransportMode.Code == "SEA") ? "BM" : "AE";

                                        seg.Content.AddRange(new[]{
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], hwb), //House Bill/
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], shipment.WayBillNumber),//HAWB No
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], "HOUSE BILL") });

                                    }
                                    else
                                    {
                                        seg.Content.AddRange(new[]{
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "MB"), //House Bill/
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], shipment.WayBillNumber),//HAWB No
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], "MASTER BILL") });
                                    }
                                    ediTrans.Content.Add(seg);
                                }
                                seg = new EdiSegment(sDef);
                                seg.Content.AddRange(new[]{
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "FI"), //Declaration Number
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], shipment.DataContext.DataSourceCollection[0].Key),
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], "DECLARATION NO") });
                                List<CustomizedField> custFieldColl = new List<CustomizedField>();
                                custFieldColl = shipment.CommercialInfo.CommercialInvoiceCollection[0].CustomizedFieldCollection.ToList();
                                CustomizedField cf = custFieldColl.Find(e => e.Key == "PO NUMBER");
                                if (!string.IsNullOrEmpty(cf.Value))
                                {
                                    ediTrans.Content.Add(seg);
                                    seg = new EdiSegment(sDef);
                                    poNumber = shipment.CommercialInfo.CommercialInvoiceCollection[0].CustomizedFieldCollection[0].Value;
                                    seg.Content.AddRange(new[]{
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "SI"), //Customer Reference No
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], poNumber),//ORDER No
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], "ORDER NO") });
                                    ediTrans.Content.Add(seg);
                                }

                                sDef = (MapSegment)map315.Content.First(s => s.Name == "Q2");
                                seg = new EdiSegment(sDef);
                                EdiSimpleDataElement q2Vessel = null;
                                EdiSimpleDataElement q2Lloyds = null;
                                EdiSimpleDataElement q2Flight = null;
                                q2Flight = new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[8], shipment.VoyageFlightNo);
                                if (seaFreight)
                                {
                                    //q2Flight = new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[8], shipment.VoyageFlightNo);
                                    if (!string.IsNullOrEmpty(shipment.LloydsIMO))
                                    {
                                        q2Lloyds = new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], shipment.LloydsIMO);
                                        q2Vessel = new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[12], shipment.VesselName);
                                    }
                                }

                                seg.Content.AddRange(new[] {
                            q2Lloyds,
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], string.Empty),
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], string.Empty),
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[3], string.Empty),
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[4], string.Empty),
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[5], string.Empty),
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[6], string.Empty),
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[7], string.Empty),
                            q2Flight,
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[9], string.Empty),
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[10], string.Empty),
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[11], seaFreight ?"L":"Z"), // Sea = Lloyds Number  });
                            q2Vessel }); // VESSEL NAME
                                ediTrans.Content.Add(seg);
                                if (held)
                                {
                                    sDef = (MapSegment)map315.Content.First(s => s.Name == "SG"); // ONLY SENT IF ON HOLD
                                    seg = new EdiSegment(sDef);

                                    seg.Content.AddRange(new[] {
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "PA"), //HOLD CODE/REASON
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], string.Empty),
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], string.Empty),
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[3], string.Format("{0:yyyyMMdd}",DateTime.Now)), //DATE OF HOLD
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[4], string.Format("{0:hhmm}",DateTime.Now)), // TIME OF HOLD
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[5], "LT") }); //LOCAL TIME 
                                    ediTrans.Content.Add(seg);
                                }


                                var loopMap = (MapLoop)map315.Content.First(s => s.Name == "L_R4"); // LOOP - PORT REFERENCE                        
                                var lDef = (MapSegment)loopMap.Content.First(s => s.Name == "R4");
                                seg = new EdiSegment(lDef);
                                seg.Content.AddRange(new[] {
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "G"), // PORT OF ENTRY
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], "UN"), // UNLOCODE 
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], shipment.PortOfFirstArrival.Code), //PORT OF ENTRY LOCODE
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[3], shipment.PortOfFirstArrival.Name), // PORT NAME
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[4], shipment.PortOfFirstArrival.Code.Substring(0,2)) }); // COUNTRY CODE
                                ediTrans.Content.Add(seg);

                                lDef = (MapSegment)loopMap.Content.First(s => s.Name == "DTM"); // LOOP - DATE TIME REFERENCE
                                List<Milestone> milestoneColl = new List<Milestone>();
                                milestoneColl = shipment.MilestoneCollection.ToList();
                                Milestone milestone = milestoneColl.Find(e => e.EventCode == "CCC");
                                if (!string.IsNullOrEmpty(milestone.ActualDate))
                                {
                                    DateTime dtMilestone;
                                    if (DateTime.TryParse(milestone.ActualDate, out dtMilestone))
                                    {
                                        seg = new EdiSegment(lDef);
                                        seg.Content.AddRange(new[] {
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "097"), // CUSTOMS START/CREATE
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], string.Format("{0:yyyyMMdd}",dtMilestone)), // DATE
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2],  string.Format("{0:hhmm}",dtMilestone)), //TIME
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[3], "LT") }); // LOCAL TIME                
                                        ediTrans.Content.Add(seg);
                                        seg = new EdiSegment(lDef);
                                        seg.Content.AddRange(new[] {
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "270"), // ENTRY FILED
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], string.Format("{0:yyyyMMdd}",dtMilestone)), // DATE
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], string.Format("{0:hhmm}",dtMilestone)), //TIME
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[3], "LT") }); // LOCAL TIME                
                                        ediTrans.Content.Add(seg);
                                    }
                                }
                                seg = new EdiSegment(lDef);
                                milestoneColl = new List<Milestone>();
                                milestoneColl = shipment.MilestoneCollection.ToList();
                                milestone = milestoneColl.Find(e => e.EventCode == "CLR");
                                if (!string.IsNullOrEmpty(milestone.ActualDate))
                                {
                                    DateTime dtMilestone;
                                    if (DateTime.TryParse(milestone.ActualDate, out dtMilestone))
                                    {
                                        seg.Content.AddRange(new[] {
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "058"), // CUSTOMS CLEARANCE
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], string.Format("{0:yyyyMMdd}",dtMilestone)), // DATE
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], string.Format("{0:hhmm}",dtMilestone)), //TIME
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[3], "LT") }); // LOCAL TIME                
                                        ediTrans.Content.Add(seg);
                                    }
                                }
                                milestoneColl = new List<Milestone>();
                                milestoneColl = shipment.MilestoneCollection.ToList();
                                milestone = milestoneColl.Find(e => e.EventCode == "CES");
                                if ((!string.IsNullOrEmpty(milestone.ActualDate)) && (milestone.ConditionReference == "HLD"))
                                {
                                    DateTime dtMilestone;
                                    if (DateTime.TryParse(milestone.ActualDate, out dtMilestone))
                                    {
                                        seg = new EdiSegment(lDef);
                                    }

                                    seg.Content.AddRange(new[] {
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "178"), // CUSTOMS HOLD
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], string.Format("{0:yyyyMMdd}",dtMilestone)), // DATE
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], string.Format("{0:hhmm}",dtMilestone)), //TIME
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[3], "LT") }); // LOCAL TIME                
                                    ediTrans.Content.Add(seg);
                                }
                                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "315 Message created for " + shipment.DataContext.DataSourceCollection[0].Key, Color.Green);

                            }
                            ediPath = WriteEdi(ediTrans, cust, poNumber, containerNo);
                            if (tslMode.Text == "Production")
                            {
                                result = SendAPL315File(ediPath, cust);
                            }
                            else
                            {
                                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " APL 315 created (" + Path.GetFileName(ediPath) + ") and (Test) sent. Ok.", Color.Green);
                                result.Processed = true;
                            }
                        }
                    }
                }
                else
                {
                    EdiSimpleDataElement b4mode = null;
                    EdiSimpleDataElement b4StatCode = null;
                    EdiSimpleDataElement b4CheckDigit = null;

                    bool held = false;
                    var sDef = (MapSegment)map315.Content.First(s => s.Name == "B4");
                    var seg = new EdiSegment(sDef);
                    if (shipment.TransportMode.Code == "SEA")
                    {
                        b4mode = new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "OOL");
                        seaFreight = true;
                    }
                    else
                    {
                        b4mode = new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "ART");
                        seaFreight = false;
                    }
                    switch (shipment.DataContext.EventType.Code)
                    {
                        case "CLR":
                            b4StatCode = new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], "CT");
                            held = false;
                            break;
                        case "CIP":
                            b4StatCode = new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], "C1");
                            held = true;
                            break;
                        case "CCC":
                            b4StatCode = new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], "GO");
                            held = true;
                            break;
                        case "CES":
                            b4StatCode = new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], "C1");
                            held = true;
                            break;
                        default:
                            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "Un-Mapped event code found :" + shipment.DataContext.EventType.Code + " Skipping file.", Color.Blue);
                            NodeResources.MoveFile(xmlFile, Globals.glFailPath);
                            result.Processed = true;
                            result.FileName = Path.GetFileName(xmlFile);
                            result.FolderLoc = Globals.glFailPath;
                            return result;
                            break;
                    }
                    seg = new EdiSegment(sDef);
                    string flightNo = shipment.VoyageFlightNo;
                    seg = new EdiSegment(sDef);
                    seg.Content.AddRange(new[] {
                                b4mode,
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], String.Empty),
                                b4StatCode,
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[3], string.Format("{0:yyyyMMdd}",DateTime.Now)), // Date of Update
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[4], string.Format("{0:hhmm}",DateTime.Now)), // Time of Update
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[5], shipment.PortOfFirstArrival.Code), // Location
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[6], string.Empty), // Sea = Container Alpha AIR = Blank
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[7], string.Empty), // Sea = Container Alpha AIR = Blank
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[8], "L"), //Load 
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[9], string.Empty),//Equipment Type
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[10], "UN"),
                                b4CheckDigit // Check Digit
                                });
                    ediTrans.Content.Add(seg);
                    sDef = (MapSegment)map315.Content.First(s => s.Name == "N9");
                    seg = new EdiSegment(sDef);
                    if (shipment.EntryHeaderCollection != null)
                    {
                        if (shipment.EntryHeaderCollection[0].EntryNumberCollection != null)
                        {
                            if (!string.IsNullOrEmpty(shipment.EntryHeaderCollection[0].EntryNumberCollection[0].Number))
                            {
                                seg.Content.AddRange(new[] {
                                        new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "6B"), //Customs Entry No
                                        new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], shipment.EntryHeaderCollection[0].EntryNumberCollection[0].Number),//Entry Number
                                        new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], "CUSTOMS-ENTRY NO") });
                                ediTrans.Content.Add(seg);
                            }
                        }
                    }



                    if (!string.IsNullOrEmpty(shipment.AdditionalBillCollection[0].BillNumber) && shipment.AdditionalBillCollection[0].BillType.Code == "MWB")
                    {
                        seg = new EdiSegment(sDef);
                        seg.Content.AddRange(new[]{
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "MB"), //Master/ OBL
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], shipment.AdditionalBillCollection[0].BillNumber),//MBL/OBL NO
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], "OCEAN BILL") });
                        ediTrans.Content.Add(seg);
                    }

                    if (!string.IsNullOrEmpty(shipment.WayBillType.Code))
                    {
                        seg = new EdiSegment(sDef);
                        if (shipment.WayBillType.Code == "HWB")
                        {
                            string hwb = (shipment.TransportMode.Code == "SEA") ? "BM" : "AE";

                            seg.Content.AddRange(new[]{
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], hwb), //House Bill/
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], shipment.WayBillNumber),//HAWB No
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], "HOUSE BILL") });

                        }
                        else
                        {
                            seg.Content.AddRange(new[]{
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "MB"), //House Bill/
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], shipment.WayBillNumber),//HAWB No
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], "MASTER BILL") });
                        }
                        ediTrans.Content.Add(seg);
                    }
                    seg = new EdiSegment(sDef);
                    seg.Content.AddRange(new[]{
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "FI"), //Declaration Number
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], shipment.DataContext.DataSourceCollection[0].Key),
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], "DECLARATION NO") });
                    List<CustomizedField> custFieldColl = new List<CustomizedField>();
                    custFieldColl = shipment.CommercialInfo.CommercialInvoiceCollection[0].CustomizedFieldCollection.ToList();
                    CustomizedField cf = custFieldColl.Find(e => e.Key == "PO NUMBER");
                    if (!string.IsNullOrEmpty(cf.Value))
                    {
                        ediTrans.Content.Add(seg);
                        seg = new EdiSegment(sDef);
                        poNumber = shipment.CommercialInfo.CommercialInvoiceCollection[0].CustomizedFieldCollection[0].Value;
                        seg.Content.AddRange(new[]{
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "SI"), //Customer Reference No
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], poNumber),//ORDER No
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], "ORDER NO") });
                        ediTrans.Content.Add(seg);
                    }

                    sDef = (MapSegment)map315.Content.First(s => s.Name == "Q2");
                    seg = new EdiSegment(sDef);
                    EdiSimpleDataElement q2Vessel = null;
                    EdiSimpleDataElement q2Lloyds = null;
                    EdiSimpleDataElement q2Flight = null;
                    q2Flight = new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[8], shipment.VoyageFlightNo);
                    seg.Content.AddRange(new[] {
                            q2Lloyds,
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], string.Empty),
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], string.Empty),
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[3], string.Empty),
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[4], string.Empty),
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[5], string.Empty),
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[6], string.Empty),
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[7], string.Empty),
                            q2Flight,
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[9], string.Empty),
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[10], string.Empty),
                            new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[11], seaFreight ?"L":"Z"), // Sea = Lloyds Number  });
                            q2Vessel }); // VESSEL NAME
                    ediTrans.Content.Add(seg);
                    if (held)
                    {
                        sDef = (MapSegment)map315.Content.First(s => s.Name == "SG"); // ONLY SENT IF ON HOLD
                        seg = new EdiSegment(sDef);

                        seg.Content.AddRange(new[] {
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "PA"), //HOLD CODE/REASON
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], string.Empty),
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], string.Empty),
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[3], string.Format("{0:yyyyMMdd}",DateTime.Now)), //DATE OF HOLD
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[4], string.Format("{0:hhmm}",DateTime.Now)), // TIME OF HOLD
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[5], "LT") }); //LOCAL TIME 
                        ediTrans.Content.Add(seg);
                    }


                    var loopMap = (MapLoop)map315.Content.First(s => s.Name == "L_R4"); // LOOP - PORT REFERENCE                        
                    var lDef = (MapSegment)loopMap.Content.First(s => s.Name == "R4");
                    seg = new EdiSegment(lDef);
                    seg.Content.AddRange(new[] {
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "G"), // PORT OF ENTRY
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], "UN"), // UNLOCODE 
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], shipment.PortOfFirstArrival.Code), //PORT OF ENTRY LOCODE
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[3], shipment.PortOfFirstArrival.Name), // PORT NAME
                                new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[4], shipment.PortOfFirstArrival.Code.Substring(0,2)) }); // COUNTRY CODE
                    ediTrans.Content.Add(seg);

                    lDef = (MapSegment)loopMap.Content.First(s => s.Name == "DTM"); // LOOP - DATE TIME REFERENCE
                    List<Milestone> milestoneColl = new List<Milestone>();
                    milestoneColl = shipment.MilestoneCollection.ToList();
                    Milestone milestone = milestoneColl.Find(e => e.EventCode == "CCC");
                    if (!string.IsNullOrEmpty(milestone.ActualDate))
                    {
                        DateTime dtMilestone;
                        if (DateTime.TryParse(milestone.ActualDate, out dtMilestone))
                        {
                            seg = new EdiSegment(lDef);
                        }

                        seg.Content.AddRange(new[] {
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "097"), // CUSTOMS START/CREATE
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], string.Format("{0:yyyyMMdd}",dtMilestone)), // DATE
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2],  string.Format("{0:hhmm}",dtMilestone)), //TIME
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[3], "LT") }); // LOCAL TIME                
                        ediTrans.Content.Add(seg);
                        seg = new EdiSegment(lDef);
                        seg.Content.AddRange(new[] {
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "270"), // ENTRY FILED
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], string.Format("{0:yyyyMMdd}",dtMilestone)), // DATE
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], string.Format("{0:hhmm}",dtMilestone)), //TIME
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[3], "LT") }); // LOCAL TIME                
                        ediTrans.Content.Add(seg);
                    }

                    seg = new EdiSegment(lDef);
                    milestoneColl = new List<Milestone>();
                    milestoneColl = shipment.MilestoneCollection.ToList();
                    milestone = milestoneColl.Find(e => e.EventCode == "CLR");
                    if (!string.IsNullOrEmpty(milestone.ActualDate))
                    {
                        DateTime dtMilestone;
                        if (DateTime.TryParse(milestone.ActualDate, out dtMilestone))
                        {
                            seg.Content.AddRange(new[] {
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "058"), // CUSTOMS CLEARANCE
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], string.Format("{0:yyyyMMdd}",dtMilestone)), // DATE
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], string.Format("{0:hhmm}",dtMilestone)), //TIME
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[3], "LT") }); // LOCAL TIME                
                        }

                        ediTrans.Content.Add(seg);
                    }

                    milestoneColl = new List<Milestone>();
                    milestoneColl = shipment.MilestoneCollection.ToList();
                    milestone = milestoneColl.Find(e => e.EventCode == "CES");
                    if ((!string.IsNullOrEmpty(milestone.ActualDate)) && (milestone.ConditionReference == "HLD"))
                    {
                        DateTime dtMilestone;
                        if (DateTime.TryParse(milestone.ActualDate, out dtMilestone))
                        {
                            seg = new EdiSegment(lDef);
                        }

                        seg.Content.AddRange(new[] {
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[0], "178"), // CUSTOMS HOLD
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[1], string.Format("{0:yyyyMMdd}",dtMilestone)), // DATE
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[2], string.Format("{0:hhmm}",dtMilestone)), //TIME
                                    new EdiSimpleDataElement((MapSimpleDataElement)sDef.Content[3], "LT") }); // LOCAL TIME                
                        ediTrans.Content.Add(seg);
                    }
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "315 Message created for " + shipment.DataContext.DataSourceCollection[0].Key, Color.Green);
                    //Add all service segments
                    ediPath = WriteEdi(ediTrans, cust, poNumber, flightNo);
                    if (tslMode.Text == "Production")
                    {
                        result = SendAPL315File(ediPath, cust);
                    }
                    else
                    {
                        NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " APL 315 created (" + Path.GetFileName(ediPath) + ") and sent. Ok.", Color.Green);
                        result.Processed = true;
                    }

                }
            }
            try
            {
                // Globals.ArchiveFile(Globals.glArcLocation, ediPath);
                if (File.Exists(ediPath))
                {
                    File.Delete(ediPath);
                }

            }
            catch (Exception ex)
            {
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + ex.GetType().Name + " Error Found when Archiving APL 315 File. Error is :" + ex.Message, Color.Red);
            }
            return result;
        }

        private ProcessResult SendAPL315File(string ediPath, CustProfileRecord cust)
        {
            ProcessResult result = new ProcessResult();
            try
            {

                WinSCP.TransferOperationResult fResult = NodeResources.FtpSend(ediPath, cust);
                fResult.Check();
                if (!fResult.IsSuccess)
                {
                    result.FileName = NodeResources.MoveFile(ediPath, Path.Combine(cust.C_path, "Failed"));
                    result.Processed = false;
                    result.FolderLoc = Path.GetDirectoryName(result.FileName);
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + fResult.Failures, Color.Red);

                }
                else
                {
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " APL 315 created (" + Path.GetFileName(ediPath) + ") and sent. Ok.", Color.Green);
                    File.Delete(ediPath);
                    result.Processed = true;
                }
            }
            catch (Exception ex)
            {
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + ex.GetType().Name + " Error Found when Sending APL 315 File. Error is :" + ex.Message, Color.Red);
            }
            return result;
        }

        private string WriteEdi(EdiTrans transEDIMessage, CustProfileRecord cust, string poNumber, string containerNo)
        {
            EdiGroup g = new EdiGroup("QO");
            g.Transactions.Add(transEDIMessage);
            var i = new EdiInterchange();
            i.Groups.Add(g);
            EdiBatch b = new EdiBatch();
            b.Interchanges.Add(i);
            EdiDataWriterSettings settings = new EdiDataWriterSettings(
                       new SegmentDefinitions.ISA(), new SegmentDefinitions.IEA(),
                       new SegmentDefinitions.GS(), new SegmentDefinitions.GE(),
                       new SegmentDefinitions.ST(), new SegmentDefinitions.SE(),
                       "ZZ", cust.P_Senderid, "ZZ", "APLUNET", cust.P_Senderid, "APLUNET",
                       "00401", "004010", "T", ediControlID("ISASERIAL"), ediControlID("GSSERIAL"), "\r\n", "*");
            EdiDataWriter w = new EdiDataWriter(settings);

            String apl315 = Path.Combine(Globals.glOutputDir, cust.P_recipientid + "315" + poNumber + containerNo + ".txt");
            int iFileCount = 0;
            try
            {
                while (File.Exists(apl315))
                {
                    iFileCount++;
                    apl315 = Path.Combine(Globals.glOutputDir, cust.P_recipientid + "315" + poNumber + containerNo + "-" + iFileCount + ".txt");
                }
                using (StreamWriter sw = new StreamWriter(apl315))
                {
                    sw.Write(w.WriteToString(b));
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + ex.GetType().Name + " Error Found when writign APL 315 File. Error is :" + ex.Message, Color.Red);
            }

            return apl315;


        }
        private int ediControlID(string serial)
        {
            Guid enumid = new Guid();
            enumid = Guid.Empty;
            int result;
            var res = NodeResources.GetEnum("EDISERIAL", serial).ToString();
            if (!string.IsNullOrEmpty(res))
            {
                result = Convert.ToInt16(res);
                enumid = NodeResources.GetEnumID("EDISERIAL", serial);
            }
            else
            {
                result = 100;

            }
            result++;
            if (enumid == Guid.Empty)
            {
                NodeResources.SetEnum("EDISERIAL", serial, result.ToString());
            }
            else
            {
                NodeResources.SetEnum("EDISERIAL", serial, result.ToString(), enumid);
            }

            return result;
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void clearLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbLog.Text = "";

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAbout AboutBox = new frmAbout();
            AboutBox.ShowDialog();
            LoadSettings();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

            LoadSettings();
            tslMode.Text = "Production";
            productionToolStripMenuItem.Checked = true;
            this.tslMain.Width = this.Width / 2;
            this.tslSpacer.Width = this.Width / 2 - (tslCmbMode.Width + productionToolStripMenuItem.Width);
            HeartBeat.RegisterHeartBeat(Globals.glCustCode, "Starting", null);

        }

        private void LoadSettings()
        {
            var appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var path = Path.Combine(appDataPath, System.Diagnostics.Process.GetCurrentProcess().ProcessName);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);


            }
            tmrMain.Interval = 60 * (1000 * 5);

            tmrMain.Elapsed += new ElapsedEventHandler(tmrMain_Elapsed);
            Globals.glAppConfig = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".XML";
            Globals.glAppConfig = Path.Combine(path, Globals.glAppConfig);
            frmSettings Settings = new frmSettings();
            if (!File.Exists(Globals.glAppConfig))
            {
                XDocument xmlConfig = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "Yes"),
                    new XElement("Satellite",
                    new XElement("Customer"),
                    new XElement("Database"),
                    new XElement("CTCNode"),
                    new XElement("Communications")));
                xmlConfig.Save(Globals.glAppConfig);
                Settings.ShowDialog();
            }
            else
            {
                try
                {
                    bool loadConfig = false;
                    XmlDocument xmlConfig = new XmlDocument();
                    xmlConfig.Load(Globals.glAppConfig);
                    // Configuration Section DataBase
                    XmlNodeList nodeList = xmlConfig.SelectNodes("/Satellite/Database");
                    XmlNode nodeCat = nodeList[0].SelectSingleNode("Catalog");
                    if (nodeCat != null)
                    {
                        Globals.glDbInstance = nodeCat.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xmlServer = nodeList[0].SelectSingleNode("Servername");
                    if (xmlServer != null)
                    {
                        Globals.glDbServer = xmlServer.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xmlUser = nodeList[0].SelectSingleNode("Username");
                    if (xmlUser != null)
                    {
                        Globals.glDbUserName = xmlUser.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xmlPassword = nodeList[0].SelectSingleNode("Password");
                    if (xmlUser != null)
                    {
                        Globals.glDbPassword = xmlPassword.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    sqlConn = new SqlConnection();
                    sqlConn.ConnectionString = Globals.connString();

                    nodeList = null;
                    //Configuration Section Customer
                    nodeList = xmlConfig.SelectNodes("/Satellite/Customer");

                    XmlNode xCustCode = nodeList[0].SelectSingleNode("Code");
                    if (xCustCode != null)
                    {
                        Globals.glCustCode = xCustCode.InnerText;
                        Globals.Customer cust = new Globals.Customer();
                        cust = Globals.GetCustomer(Globals.glCustCode);
                        if (cust != null)
                        {
                            Globals.glCustCode = cust.Code;
                            Globals.gl_CustId = cust.Id;
                        }
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xPath = nodeList[0].SelectSingleNode("ProfilePath");
                    if (xPath != null)
                    {
                        Globals.glProfilePath = xPath.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xTestPath = nodeList[0].SelectSingleNode("TestPath");
                    if (xTestPath != null)
                    {
                        Globals.glTestLocation = xTestPath.InnerText;
                    }
                    XmlNode xArchivePath = nodeList[0].SelectSingleNode("ArchiveLocation");
                    if (xArchivePath != null)
                    {
                        Globals.glArcLocation = xArchivePath.InnerText;
                    }
                    XmlNode xCustomPath = nodeList[0].SelectSingleNode("CustomFilesPath");
                    if (xCustomPath != null)
                    {
                        Globals.glCustomFilesPath = xCustomPath.InnerText;
                    }

                    XmlNode xOutPath = nodeList[0].SelectSingleNode("OutputPath");
                    if (xOutPath != null)
                    {
                        Globals.glOutputDir = xOutPath.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xInPath = nodeList[0].SelectSingleNode("PickupPath");
                    if (xInPath != null)
                    {
                        Globals.glPickupPath = xInPath.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xLib = nodeList[0].SelectSingleNode("LibraryPath");
                    if (xLib != null)
                    {
                        Globals.glLibPath = xLib.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xFail = nodeList[0].SelectSingleNode("FailPath");
                    if (xFail != null)
                    {
                        Globals.glFailPath = xFail.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    if (loadConfig)
                    {
                        throw new System.Exception("Mandatory Settings missing");
                    }

                    // Configuration Section Communications
                    nodeList = xmlConfig.SelectNodes("/Satellite/Communications");

                    XmlNode xmlAlertsTo = nodeList[0].SelectSingleNode("AlertsTo");
                    if (xmlAlertsTo != null)
                    {
                        Globals.glAlertsTo = xmlAlertsTo.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xSmtp = nodeList[0].SelectSingleNode("SMTPServer");
                    if (xSmtp != null)
                    {
                        SMTPServer.Server = xSmtp.InnerText;
                        SMTPServer.Port = xSmtp.Attributes[0].InnerXml;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xEmail = nodeList[0].SelectSingleNode("EmailAddress");
                    if (xEmail != null)
                    { SMTPServer.Email = xEmail.InnerText; }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xUserName = nodeList[0].SelectSingleNode("SMTPUsername");
                    if (xUserName != null)
                    {
                        SMTPServer.Username = xUserName.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xPassword = nodeList[0].SelectSingleNode("SMTPPassword");
                    if (xPassword != null)
                    {
                        SMTPServer.Password = xPassword.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }
                    //CTC Node Database Connection
                    nodeList = null;
                    nodeList = xmlConfig.SelectNodes("/Satellite/CTCNode");
                    nodeCat = null;
                    nodeCat = nodeList[0].SelectSingleNode("Catalog");
                    if (nodeCat != null)
                    {
                        Globals.glCTCDbInstance = nodeCat.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    xmlServer = null;
                    xmlServer = nodeList[0].SelectSingleNode("Servername");
                    if (xmlServer != null)
                    {
                        Globals.glCTCDbServer = xmlServer.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    xmlUser = null;
                    xmlUser = nodeList[0].SelectSingleNode("Username");
                    if (xmlUser != null)
                    {
                        Globals.glCTCDbUserName = xmlUser.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    xmlPassword = null;
                    xmlPassword = nodeList[0].SelectSingleNode("Password");
                    if (xmlUser != null)
                    {
                        Globals.glCTCDbPassword = xmlPassword.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    sqlCTCConn = new SqlConnection();
                    sqlCTCConn.ConnectionString = Globals.CTCconnString();
                }

                catch (Exception)
                {
                    MessageBox.Show("There was an error loading the Config files. Please check the settings", "Loading Settings", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    frmSettings FormSettings = new frmSettings();
                    FormSettings.ShowDialog();
                    DialogResult dr = FormSettings.DialogResult;
                    if (dr == DialogResult.OK)
                    {
                        LoadSettings();
                    }
                }
                var Cust = Globals.GetCustomer(Globals.glCustCode);
                try
                {
                    this.Text = "CTC Satellite - " + Cust.CustomerName;
                }
                catch (Exception)
                {
                    MessageBox.Show("There was an error loading the Config files. Please check the settings", "Loading Settings", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    frmSettings FormSettings = new frmSettings();
                    FormSettings.ShowDialog();
                    DialogResult dr = FormSettings.DialogResult;
                    if (dr == DialogResult.OK)
                    {
                        LoadSettings();
                    }
                }

                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "System Ready");
            }
        }



        private void profilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmProfiles Profiles = new frmProfiles();
            Profiles.ShowDialog();

        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSettings FormSettings = new frmSettings();
            FormSettings.ShowDialog();
            DialogResult dr = FormSettings.DialogResult;
            if (dr == DialogResult.OK)
            {
                LoadSettings();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GetFiles();

        }

        private void GetFiles()
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());

            string filesPath = string.Empty;
            if (tslMode.Text == "Production")
            {
                filesPath = Globals.glPickupPath;
            }
            else
            {
                filesPath = Globals.glTestLocation;
            }
            DirectoryInfo diTodo = new DirectoryInfo(filesPath);
            int iCount = 0;
            var stopwatch = new Stopwatch();

            var list = diTodo.GetFiles();
            if (list.Length > 0)
            {
                stopwatch.Start();
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Now Processing " + list.Length + " Files");
                foreach (var fileName in diTodo.GetFiles())
                {
                    iCount++;
                    ProcessResult thisResult = new ProcessResult();
                    thisResult = ProcessCustomFiles(fileName.FullName);
                    //return;
                    if (thisResult.Processed)
                    {
                        try
                        {
                            if (File.Exists(fileName.FullName))
                            {
                                System.GC.Collect();
                                System.GC.WaitForPendingFinalizers();
                                File.Delete(fileName.FullName);
                            }
                        }
                        catch (Exception ex)
                        {
                            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Unable to Delete File. Error :" + ex.Message);
                        }

                    }

                }
                stopwatch.Stop();
                double t = stopwatch.Elapsed.TotalSeconds;
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Processing Complete. " + iCount + " files processed in " + t + " seconds");
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Waiting for files.");
            }

        }

        private ProcessResult ProcessCustomFiles(String xmlFile)
        {
            ProcessResult thisResult = new ProcessResult();
            thisResult.Processed = true;


            FileInfo processingFile = new FileInfo(xmlFile);
            filesRx++;
            NodeResources.AddLabel(lblFilesRx, filesRx.ToString());
            totfilesRx++;
            NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
            TransReference trRef = new TransReference();

            switch (processingFile.Extension.ToUpper())
            {
                case ".XML":

                    thisResult = ProcessCargowiseFiles(xmlFile);

                    break;
                case ".CSV":
                    break;
                case ".TXT":
                    thisResult = ProcessCustomXML(xmlFile);
                    break;
                default:
                    NodeResources.MoveFile(xmlFile, Globals.glFailPath);
                    break;

            }

            return thisResult;
        }



        private ProcessResult ProcessUDM(string xmlfile)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            FileInfo processingFile = new FileInfo(xmlfile);
            ProcessResult thisResult = new ProcessResult();
            thisResult.Processed = true;
            CW.UniversalInterchange cwFile = new CW.UniversalInterchange();
            using (FileStream fStream = new FileStream(xmlfile, FileMode.Open))
            {
                XmlSerializer cwConvert = new XmlSerializer(typeof(CW.UniversalInterchange));
                cwFile = (CW.UniversalInterchange)cwConvert.Deserialize(fStream);
                fStream.Close();
            }
            if (cwFile.Body.BodyField == null)
            {
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Cargowise File found but is not UDM. Attempting NDM: ");
                //thisResult = ProcessProductResponse(xmlfile);
                return thisResult;
            }
            totfilesRx++;
            NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
            cwRx++;
            NodeResources.AddLabel(lblCwRx, cwRx.ToString());
            String senderID = String.Empty;
            String recipientID = String.Empty;
            String reasonCode = String.Empty;
            String eventCode = String.Empty;
            TransReference trRef = new TransReference();
            List<CW.DataSource> dscoll = new List<CW.DataSource>();
            CW.DataContext dc = new CW.DataContext();
            dc = cwFile.Body.BodyField.Shipment.DataContext;

            if (dc.DataSourceCollection != null)
            {
                try
                {
                    //dscoll = cwFile.Body.BodyField.Shipment.DataContext.DataSourceCollection.ToList();
                    dscoll = cwFile.Body.BodyField.Shipment.DataContext.DataSourceCollection.ToList();
                }
                catch (Exception)
                {

                    NodeResources.AddText(rtbLog, "XML File Missing DataSourceCollection tag");
                    ProcessingErrors procerror = new ProcessingErrors();
                    CTCErrorCode error = new CTCErrorCode();
                    error.Code = NodeError.e111;
                    error.Description = "DataSource Collection not found:";
                    error.Severity = "Fatal";
                    procerror.ErrorCode = error;
                    procerror.SenderId = senderID;
                    procerror.RecipientId = recipientID;
                    procerror.FileName = xmlfile;
                    procerror.ProcId = Guid.Empty;

                    //AddProcessingError(procerror);
                    thisResult.FolderLoc = Globals.glFailPath;
                    thisResult.Processed = false;
                    thisResult.FileName = xmlfile;
                    return thisResult;
                }

            }
            try
            {
                cwRx++;
                NodeResources.AddLabel(lblCwRx, cwRx.ToString());
                senderID = cwFile.Header.SenderID;
                recipientID = cwFile.Header.RecipientID;
                if (cwFile.Body.BodyField.Shipment.DataContext.EventType != null)
                {
                    eventCode = cwFile.Body.BodyField.Shipment.DataContext.EventType.Code;
                }
                else
                {
                    eventCode = string.Empty;
                }
                if (cwFile.Body.BodyField.Shipment.DataContext.ActionPurpose != null)
                {
                    reasonCode = cwFile.Body.BodyField.Shipment.DataContext.ActionPurpose.Code;
                }
                else
                {
                    reasonCode = string.Empty;
                }
                //reasonCode = cwFile.Body.BodyField.Shipment.DataContext.ActionPurpose.Code;
                CustProfileRecord custProfileRecord = new CustProfileRecord();
                custProfileRecord = GetCustomerProfile(senderID, recipientID, reasonCode, eventCode);
                if (custProfileRecord.C_name != null)
                {
                    //Is Client Active and not on hold
                    if (custProfileRecord.C_is_active == "N" | custProfileRecord.C_on_hold == "Y")
                    {

                        string f = NodeResources.MoveFile(xmlfile, Path.Combine(custProfileRecord.C_path, "Held"));
                        if (f.StartsWith("Warning"))
                        {
                            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                        }
                        else
                        { xmlfile = f; }
                        NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfileRecord.C_name + " is not Active or is on Hold. Moving to Hold and skipping" + "");
                        thisResult.Processed = true;
                        thisResult.FileName = xmlfile;
                        return thisResult;
                    }
                    else
                    {
                        switch (custProfileRecord.P_MsgType.Trim())
                        {

                            case "Conversion":
                                thisResult = ConvertCWFile(custProfileRecord, xmlfile);
                                break;
                            case "Original":
                                NodeResources.MoveFile(xmlfile, Globals.glFailPath);
                                thisResult.FileName = xmlfile;
                                thisResult.FolderLoc = Globals.glFailPath;
                                thisResult.Processed = false;
                                break;
                        }
                        return thisResult;
                    }
                }
            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;
            }
            return thisResult;

        }

        private ProcessResult ConvertCWFile(CustProfileRecord cust, String file)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            ProcessResult result = new ProcessResult();
            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "Beginning Cargowise Conversion: " + file);


            if (!String.IsNullOrEmpty(cust.P_Method))
            {
                string custMethod;
                custMethod = cust.P_Method;
                string custParams = cust.P_Params;
                Type custType = this.GetType();
                MethodInfo custMethodToRun = custType.GetMethod(custMethod);
                try
                {
                    // Create315(file, cust);
                    if (File.Exists(file))
                    {
                        var varResult = custMethodToRun.Invoke(this, new Object[] { file, cust });
                        if (varResult != null)
                        {
                            //AddTransaction((Guid)dr["C_ID"], (Guid)dr["P_ID"], xmlFile, msgDir, true, (TransReference)varResult, archiveFile);

                            result.FileName = file;

                            // Create315(file, cust);
                            result.Processed = true;
                        }
                    }

                }
                catch (Exception ex)
                {
                    string strEx = ex.GetType().Name;
                    strEx += " " + ex.Message;
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "Error Found during " + cust.P_Method + ": " + strEx, Color.Red);
                    result.FileName = file;
                    result.FolderLoc = Globals.glFailPath;
                    result.Processed = false;
                    NodeResources.MoveFile(file, Globals.glFailPath);
                }


            }
            return result;


        }

        public ProcessResult APL_ImportOFWPurchaseOrder(string xmlFile, CustProfileRecord cust)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            ProcessResult result = new ProcessResult();

            ZRZZORD5 ofwOrder = new ZRZZORD5();
            FileInfo fileInfo = new FileInfo(xmlFile);
            ProcessResult thisResult = new ProcessResult();
            string officeworksOrderNo = string.Empty;
            string officeworksSupplier = string.Empty;
            using (FileStream fs = new FileStream(xmlFile, FileMode.Open, FileAccess.Read))
            {
                XmlSerializer xs = new XmlSerializer(typeof(ZRZZORD5));
                ofwOrder = (ZRZZORD5)xs.Deserialize(fs);
                fs.Close();
            }
            CWShipment cwCommInvoiceXML = new CWShipment();
            DataTarget dataTarget = new DataTarget();
            dataTarget.Type = "CustomsCommercialInvoice";
            List<DataTarget> dataTargetColl = new List<DataTarget>();
            dataTargetColl.Add(dataTarget);
            DataContext dataContext = new DataContext();
            dataContext.DataTargetCollection = dataTargetColl.ToArray();
            cwCommInvoiceXML.DataContext = dataContext;
            CommercialInfo commercialInfo = new CommercialInfo();
            commercialInfo.Name = "All Invoices";
            List<CommercialInfoCommercialInvoice> commericalInvoiceColl = new List<CommercialInfoCommercialInvoice>();
            CommercialInfoCommercialInvoice civHeader = new CommercialInfoCommercialInvoice();
            List<ZRZZORD5IDOCE1EDK02> ordHeaderColl = new List<ZRZZORD5IDOCE1EDK02>();
            List<ZRZZORD5IDOCE1EDKA1> OrgColl = new List<ZRZZORD5IDOCE1EDKA1>();
            List<ZRZZORD5IDOCE1EDK02> OrderColl = new List<ZRZZORD5IDOCE1EDK02>();
            List<ZRZZORD5IDOCE1EDK03> dateColl = new List<ZRZZORD5IDOCE1EDK03>();
            List<ZRZZORD5IDOCE1EDK17> ordTermsColl = new List<ZRZZORD5IDOCE1EDK17>();
            List<ZRZZORD5IDOCE1EDP01> orderLinesColl = new List<ZRZZORD5IDOCE1EDP01>();
            List<ZRZZORD5IDOCE1EDS01> ordSumcoll = new List<ZRZZORD5IDOCE1EDS01>();
            try
            {
                if (ofwOrder.IDOC.E1EDK02 != null)
                {
                    ordHeaderColl = ofwOrder.IDOC.E1EDK02.ToList();
                }
                else
                {
                    throw new MissingElementException("ordHeaderColl");
                }

                if (ofwOrder.IDOC.E1EDKA1 != null)
                {
                    OrgColl = ofwOrder.IDOC.E1EDKA1.ToList();
                }
                else
                {
                    throw new MissingElementException("OrgColl");
                }

                if (ofwOrder.IDOC.E1EDK17 != null)
                {
                    ordTermsColl = ofwOrder.IDOC.E1EDK17.ToList();
                }
                else
                {
                    throw new MissingElementException("ordTermsColl");
                }

                if (ofwOrder.IDOC.E1EDS01 != null)
                {
                    ordSumcoll = ofwOrder.IDOC.E1EDS01.ToList();
                }
                else
                {
                    throw new MissingElementException("ordSumcoll");
                }

                if (ofwOrder.IDOC.E1EDP01 != null)
                {
                    orderLinesColl = ofwOrder.IDOC.E1EDP01.ToList();
                }
                else
                {
                    throw new MissingElementException("orderLinesColl");
                }

                try
                {
                    foreach (ZRZZORD5IDOCE1EDK02 header in ordHeaderColl)
                    {


                        switch (header.QUALF)
                        {
                            case "001":
                                officeworksOrderNo = header.BELNR;
                                civHeader.InvoiceNumber = officeworksOrderNo;
                                List<CustomizedField> headerCustFieldColl = new List<CustomizedField>();
                                CustomizedField headerCustField = new CustomizedField();

                                headerCustField.Key = "PO NUMBER";
                                headerCustField.Value = officeworksOrderNo;
                                headerCustFieldColl.Add(headerCustField);
                                civHeader.CustomizedFieldCollection = headerCustFieldColl.ToArray();
                                break;
                            case "007":
                                break;

                        }
                    }

                    foreach (ZRZZORD5IDOCE1EDKA1 org in OrgColl)
                    {
                        switch (org.PARVW)
                        {
                            case "LF":  //Vendor
                                OrganizationAddress supplier = new OrganizationAddress();
                                supplier.AddressType = "Supplier";
                                supplier.CompanyName = org.NAME1;
                                if (string.IsNullOrEmpty(NodeResources.GetEnum("OrgCode", org.PARTN)))
                                {
                                    result.Processed = false;
                                    result.FolderLoc = Globals.glFailPath;
                                    string errMessage = "Please note Was unable to import and convert Purchase Order: " + officeworksOrderNo + Environment.NewLine;
                                    errMessage += "Reason: Unable to Locate Supplier details." + Environment.NewLine;
                                    errMessage += "Supplier Name: " + org.NAME1 + Environment.NewLine;
                                    errMessage += "Address: " + org.STRAS + Environment.NewLine;
                                    errMessage += org.ORT01 + Environment.NewLine;
                                    errMessage += "State: " + org.REGIO + Environment.NewLine;
                                    errMessage += "Post Code: " + org.PSTLZ + Environment.NewLine;
                                    errMessage += "Country: " + org.LAND1 + Environment.NewLine;
                                    errMessage += "Office Works Code: " + org.PARTN + Environment.NewLine;
                                    errMessage += "Paste Cargowise Code here:" + Environment.NewLine;
                                    errMessage += "Please forward to support@ctcorp.com.au ";

                                    MailModule.sendMsg(xmlFile, Globals.glAlertsTo, "Unable to find Supplier details. Order No:" + officeworksOrderNo, errMessage);
                                    string failedFile = NodeResources.MoveFile(xmlFile, Globals.glFailPath);
                                    TransReference failedRef = new TransReference();
                                    failedRef.Reference1 = officeworksOrderNo;
                                    failedRef.Reference2 = org.PARTN;
                                    failedRef.Reference3 = Globals.glFailPath;
                                    failedRef.Ref1Type = TransReference.RefType.Order;
                                    failedRef.Ref2Type = TransReference.RefType.Custom;
                                    failedRef.Ref3Type = TransReference.RefType.Error;
                                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "Buyer Code is missing " + org.PARTN + ": for Order: " + officeworksOrderNo, Color.Red);
                                    NodeResources.AddTransaction(NodeResources.GetCustID(cust.P_recipientid), Guid.Empty, failedFile, "S", true, failedRef, string.Empty);
                                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "Missing Organisation Mapping for : " + org.NAME1 + "(" + org.PARTN + "). Message sent and file is being skipped", Color.Orange);
                                    return result;
                                }
                                else
                                {
                                    supplier.OrganizationCode = NodeResources.GetEnum("OrgCode", org.PARTN);
                                    officeworksSupplier = org.PARTN;
                                }


                                if (org.REGIO != null)
                                {
                                    supplier.State = org.REGIO;
                                }
                                if (org.PSTLZ != null)
                                {
                                    supplier.Postcode = org.PSTLZ;
                                }
                                supplier.Address1 = org.STRAS;
                                if (!string.IsNullOrEmpty(org.ORT01))
                                {
                                    bool hasNumbers = org.ORT01.All(char.IsDigit);
                                    if (hasNumbers)
                                    {
                                        supplier.Address2 = org.ORT01;
                                    }
                                    else
                                    {
                                        supplier.City = org.ORT01;
                                    }
                                }


                                supplier.Country = new Country { Code = org.LAND1 };
                                List<RegistrationNumber> regNocoll = new List<RegistrationNumber>();
                                RegistrationNumber regNo = new RegistrationNumber();
                                regNo.CountryOfIssue = new Country { Code = "AU" };
                                regNo.Type = new RegistrationNumberType { Code = "LSC" };
                                regNo.Value = org.PARTN;

                                regNocoll.Add(regNo);
                                supplier.RegistrationNumberCollection = regNocoll.ToArray();
                                civHeader.Supplier = supplier;

                                break;
                            case "SP":  //??
                                break;
                            case "WE":  //Delivery Address

                                break;
                            case "BA":  //??
                                break;
                            case "AG":  //Consignee
                                OrganizationAddress buyer = new OrganizationAddress();
                                buyer.AddressType = "Buyer";
                                buyer.CompanyName = org.ORGTX;
                                if (string.IsNullOrEmpty(NodeResources.GetEnum("OrgCode", org.PARTN)))
                                {
                                    result.Processed = false;
                                    result.FolderLoc = Globals.glFailPath;
                                    string errMessage = "Please note Was unable to import and convert Purchase Order: " + officeworksOrderNo + Environment.NewLine;
                                    errMessage += "By Default the Buyer is always OFFICEMEL. However please confirm. " + Environment.NewLine;
                                    errMessage += "----------------------------------------------------------------------" + Environment.NewLine;
                                    errMessage += "Cargowise Code (Change as Necessary): OFFICEMEL";
                                    errMessage += "Reason: Unable to Locate BUYER details." + Environment.NewLine;
                                    errMessage += "Buyer Name: " + org.ORGTX + Environment.NewLine;
                                    errMessage += "Address: " + org.STRAS + Environment.NewLine;
                                    errMessage += org.ORT01 + Environment.NewLine;
                                    errMessage += "State: " + org.REGIO + Environment.NewLine;
                                    errMessage += "Post Code: " + org.PSTLZ + Environment.NewLine;
                                    errMessage += "Country: " + org.LAND1 + Environment.NewLine;
                                    errMessage += "Office Works Code: " + org.PARTN + Environment.NewLine;
                                    errMessage += "Please create Cargowise Code and report back to CTC with the Cargowise Code when done.";
                                    MailModule.sendMsg("", Globals.glAlertsTo, "Buyer Details have changed for Order " + officeworksOrderNo, errMessage);
                                    string failedFile = NodeResources.MoveFile(xmlFile, Globals.glFailPath);
                                    TransReference failedRef = new TransReference();
                                    failedRef.Reference1 = officeworksOrderNo;
                                    failedRef.Reference2 = org.PARTN;
                                    failedRef.Reference3 = Globals.glFailPath;
                                    failedRef.Ref1Type = TransReference.RefType.Order;
                                    failedRef.Ref2Type = TransReference.RefType.Custom;
                                    failedRef.Ref3Type = TransReference.RefType.Error;
                                    NodeResources.AddTransaction(NodeResources.GetCustID(cust.P_recipientid), Guid.Empty, failedFile, "S", true, failedRef, string.Empty);
                                    return result;
                                }
                                else
                                {
                                    buyer.OrganizationCode = NodeResources.GetEnum("OrgCode", org.PARTN);
                                }


                                List<RegistrationNumber> buyerregNocoll = new List<RegistrationNumber>();
                                RegistrationNumber buyerregNo = new RegistrationNumber();
                                buyerregNo.CountryOfIssue = new Country { Code = "AU" };
                                buyerregNo.Type = new RegistrationNumberType { Code = "LSC" };
                                buyerregNo.Value = org.PARTN;
                                buyerregNocoll.Add(buyerregNo);
                                buyer.RegistrationNumberCollection = buyerregNocoll.ToArray();

                                civHeader.Buyer = buyer;
                                break;
                        }

                    }

                    foreach (ZRZZORD5IDOCE1EDK17 terms in ordTermsColl)
                    {
                        switch (terms.QUALF)
                        {
                            case "001":
                                if (terms.LKOND != null)
                                {
                                    civHeader.IncoTerm = new CodeDescriptionPair { Code = terms.LKOND };
                                }

                                break;
                            case "002":
                                break;
                        }
                    }

                    foreach (ZRZZORD5IDOCE1EDS01 ordSumm in ordSumcoll)
                    {
                        switch (ordSumm.SUMID)
                        {
                            case "002":
                                civHeader.InvoiceAmount = decimal.Parse(ordSumm.SUMME);
                                civHeader.InvoiceCurrency = new Currency { Code = ordSumm.SUNIT };
                                break;
                        }
                    }

                    List<CommercialInfoCommercialInvoiceCommercialInvoiceLine> invLineColl = new List<CommercialInfoCommercialInvoiceCommercialInvoiceLine>();
                    int lineCnt = 1;
                    foreach (ZRZZORD5IDOCE1EDP01 orderLine in orderLinesColl)
                    {
                        CommercialInfoCommercialInvoiceCommercialInvoiceLine civLine = new CommercialInfoCommercialInvoiceCommercialInvoiceLine();
                        civLine.LineNo = lineCnt;
                        civLine.LineNoSpecified = true;
                        civLine.Link = lineCnt;
                        civLine.LinkSpecified = true;
                        decimal val = 0;
                        if (decimal.TryParse(orderLine.VPREI, out val))
                        {
                            civLine.UnitPrice = val;
                            civLine.UnitPriceSpecified = true;
                        }
                        val = 0;
                        if (decimal.TryParse(orderLine.NETWR, out val))
                        {
                            civLine.LinePrice = val;
                            civLine.LinePriceSpecified = true;

                            civLine.CustomsValue = val;
                            civLine.CustomsValueSpecified = true;
                        }
                        val = 0;
                        if (decimal.TryParse(orderLine.MENGE, out val))
                        {
                            civLine.InvoiceQuantity = val;
                            civLine.InvoiceQuantitySpecified = true;
                        }

                        civLine.CustomsQuantityUnit = new CodeDescriptionPair { Code = NodeResources.GetEnum("CUSTUOM", orderLine.PMENE) };
                        civLine.InvoiceQuantityUnit = new CodeDescriptionPair { Code = NodeResources.GetEnum("UOM", orderLine.PMENE) };
                        val = 0;
                        if (decimal.TryParse(orderLine.Z1EDPV1.VOLUM, out val))
                        {
                            civLine.Volume = val;
                            civLine.VolumeSpecified = true;
                            civLine.VolumeUnit = new UnitOfVolume { Code = NodeResources.GetEnum("UOM", orderLine.Z1EDPV1.VOLEH) };
                        }
                        val = 0;
                        if (decimal.TryParse(orderLine.BRGEW, out val))
                        {
                            civLine.Weight = val;
                            civLine.WeightSpecified = true;
                            civLine.WeightUnit = new UnitOfWeight { Code = NodeResources.GetEnum("UOM", orderLine.GEWEI) };
                        }
                        foreach (ZRZZORD5IDOCE1EDP01E1EDP19 itemDetail in orderLine.E1EDP19)
                        {
                            switch (itemDetail.QUALF)
                            {
                                case "001":
                                    civLine.PartNo = itemDetail.IDTNR;
                                    civLine.Description = itemDetail.KTEXT;
                                    break;
                                case "003":
                                    break;
                                case "002":
                                    break;
                            }
                        }
                        List<CustomizedField> custFieldColl = new List<CustomizedField>();
                        CustomizedField custField = new CustomizedField();
                        custField.Key = "Item Number";
                        custField.DataType = CustomizedFieldDataType.String;
                        custField.Value = orderLine.POSEX;
                        custFieldColl.Add(custField);
                        civLine.CustomizedFieldCollection = custFieldColl.ToArray();
                        lineCnt++;
                        invLineColl.Add(civLine);

                    }
                    civHeader.CommercialInvoiceLineCollection = invLineColl.ToArray();
                    commericalInvoiceColl.Add(civHeader);
                    commercialInfo.CommercialInvoiceCollection = commericalInvoiceColl.ToArray();
                    cwCommInvoiceXML.CommercialInfo = commercialInfo;
                    cwCommInvoiceXML.MessageType = new CodeDescriptionPair { Code = "IMP" };
                    UniversalInterchange uInt = new UniversalInterchange();
                    UniversalInterchangeHeader uHeader = new UniversalInterchangeHeader();
                    uHeader.RecipientID = "INISYDSYD";
                    uHeader.SenderID = "OFFICEMEL";
                    uInt.Header = uHeader;
                    UniversalInterchangeBody uBody = new UniversalInterchangeBody();
                    UniversalShipmentData uShipData = new UniversalShipmentData();
                    uBody.BodyField = uShipData;
                    uShipData.version = "1.1";
                    uShipData.Shipment = cwCommInvoiceXML;
                    String cwXML = Path.Combine(Globals.glOutputDir, "OFFICEMEL" + "PO" + officeworksOrderNo.Trim() + ".xml");
                    int iFileCount = 0;
                    while (File.Exists(cwXML))
                    {
                        iFileCount++;
                        cwXML = Path.Combine(Globals.glOutputDir, "OFFICEMEL" + "PO" + officeworksOrderNo.Trim() + iFileCount + ".xml");
                    }
                    Stream outputCW = File.Open(cwXML, FileMode.Create);
                    StringWriter writer = new StringWriter();
                    uInt.Body = uBody;
                    XmlSerializer xSer = new XmlSerializer(typeof(UniversalInterchange));
                    var cwNSUniversal = new XmlSerializerNamespaces();
                    cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
                    xSer.Serialize(outputCW, uInt, cwNSUniversal);
                    outputCW.Flush();
                    outputCW.Close();
                    //string archive = Globals.ArchiveFile(Globals.glArcLocation, cwXML);
                    TransReference tRef = new TransReference();
                    tRef.Ref1Type = TransReference.RefType.Order;
                    tRef.Reference1 = officeworksOrderNo;
                    tRef.Ref2Type = TransReference.RefType.Custom;
                    tRef.Reference2 = officeworksSupplier;
                    tRef.Reference3 = string.Empty;
                    tRef.Ref3Type = TransReference.RefType.Brokerage;
                    NodeResources.AddTransaction(NodeResources.GetCustID(uHeader.RecipientID), Guid.Empty, cwXML, "S", true, tRef, "");
                    string archive = Globals.ArchiveFile(Globals.glArcLocation, xmlFile);
                    if (string.IsNullOrEmpty(archive))
                    {

                        string errMsg = "Unable to find Archive Name " + xmlFile + Environment.NewLine;
                        errMsg += "Archive should be set but is blank/empty: " + Environment.NewLine;
                        MailModule.sendMsg(xmlFile, "andy.duggan@ctcorp.com.au", cust.C_code + ": Archive Settings are blank", errMsg);
                    }
                    NodeResources.AddTransaction(NodeResources.GetCustID(uHeader.RecipientID), Guid.Empty, xmlFile, "S", true, tRef, archive);
                }
                catch (Exception ex)
                {
                    string strEx = ex.GetType().Name;

                    string errMsg = "Error found when processing " + xmlFile + Environment.NewLine;
                    errMsg += "Error Type: " + strEx + Environment.NewLine;
                    errMsg += "Message: " + ex.Message + Environment.NewLine;
                    errMsg += "Stack: " + ex.StackTrace + Environment.NewLine;
                    errMsg += "Inner: " + ex.InnerException + Environment.NewLine;
                    errMsg += "Source: " + ex.Source;
                    MailModule.sendMsg(string.Empty, Globals.glAlertsTo, cust.C_code + ": Unable to process Officeworks Order", errMsg);
                    NodeResources.MoveFile(xmlFile, Globals.glFailPath);
                    result.FileName = xmlFile;
                    result.FolderLoc = Globals.glFailPath;
                    result.Processed = false;
                }
            }

            catch (MissingElementException ex)
            {
                string strEx = ex.GetType().Name;
                string errMsg = "Error found when processing " + xmlFile + Environment.NewLine;
                errMsg += "Missing XML Node: " + ex.Message.ToUpper() + Environment.NewLine;
                errMsg += "Unable to create Cargowise Commercial Invoice for Purchase Order : ";
                MailModule.sendMsg(xmlFile, cust.P_NotifyEmail + ";" + Globals.glAlertsTo, cust.C_code + ": Unable to process Officeworks Order", errMsg);
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "INISYDSYD:Unable to Process Officeworks Order", Color.Red);
                NodeResources.MoveFile(xmlFile, Globals.glFailPath);
                result.FileName = xmlFile;
                result.FolderLoc = Globals.glFailPath;
                result.Processed = false;
            }
            catch (ArgumentNullException ex)
            {
                string strEx = ex.GetType().Name;
                string errMsg = "Error found when processing " + xmlFile + Environment.NewLine;
                errMsg += "Error Type: " + strEx + Environment.NewLine;
                errMsg += "Message: " + ex.Message + Environment.NewLine;
                errMsg += "Stack: " + ex.StackTrace + Environment.NewLine;
                errMsg += "Inner: " + ex.InnerException + Environment.NewLine;
                errMsg += "Source: " + ex.Source;
                MailModule.sendMsg(xmlFile, Globals.glAlertsTo, cust.C_code + ": Unable to process Officeworks Order", errMsg);
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "INISYDSYD:Unable to Process Officeworks Order", Color.Red);
                NodeResources.MoveFile(xmlFile, Globals.glFailPath);
                result.FileName = xmlFile;
                result.FolderLoc = Globals.glFailPath;
                result.Processed = false;
            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;

                string errMsg = "Error found when processing " + xmlFile + Environment.NewLine;
                errMsg += "Error Type: " + strEx + Environment.NewLine;
                errMsg += "Message: " + ex.Message + Environment.NewLine;
                errMsg += "Stack: " + ex.StackTrace + Environment.NewLine;
                errMsg += "Inner: " + ex.InnerException + Environment.NewLine;
                errMsg += "Source: " + ex.Source;
                MailModule.sendMsg(xmlFile, Globals.glAlertsTo, cust.C_code + ": Unable to process Officeworks Order", errMsg);
                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "INISYDSYD:Unable to Process Officeworks Order", Color.Red);
                NodeResources.MoveFile(xmlFile, Globals.glFailPath);
                result.FileName = xmlFile;
                result.FolderLoc = Globals.glFailPath;
                result.Processed = false;
            }




            return result;


        }



        public ProcessResult CreateOfficeWorksFile(string xmlFile, CustProfileRecord cust)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            ProcessResult result = new ProcessResult();
            PORDCH03 officeWorksOrder = new PORDCH03();
            CW.UniversalInterchange cwFile = new CW.UniversalInterchange();
            using (FileStream fStream = new FileStream(xmlFile, FileMode.Open))
            {
                XmlSerializer cwConvert = new XmlSerializer(typeof(CW.UniversalInterchange));
                cwFile = (CW.UniversalInterchange)cwConvert.Deserialize(fStream);
                fStream.Close();
            }
            CW.Shipment shipment = new CWShipment();
            shipment = cwFile.Body.BodyField.Shipment;
            var dec = (from d in cwFile.Body.BodyField.Shipment.DataContext.DataSourceCollection
                       where d.Type == "CustomsDeclaration"
                       select d.Key).SingleOrDefault();

            CW.CommercialInfo commercialInfo = new CW.CommercialInfo();
            PORDCH03IDOC idoc = new PORDCH03IDOC();
            idoc.BEGIN = PORDCH03IDOCBEGIN.Item1;

            officeWorksOrder.IDOC = idoc;
            PORDCH03IDOCEDI_DC40 edi_40 = new PORDCH03IDOCEDI_DC40();
            edi_40.TABNAM = "";
            edi_40.MANDT = "011";
            edi_40.DIRECT = PORDCH03IDOCEDI_DC40DIRECT.Item2;
            edi_40.IDOCTYP = "PORDCH03";
            edi_40.CIMTYP = "";
            edi_40.MESTYP = "PORDCH";
            edi_40.MESCOD = "";
            edi_40.SNDPOR = "SAPPIP";
            edi_40.SNDPRT = "LS";
            edi_40.SNDPRN = "APLL";
            edi_40.RCVPOR = "SAPR1P";
            edi_40.RCVPRT = "LS";
            edi_40.RCVPRN = "R1PCLNT011";
            edi_40.CREDAT = DateTime.Now.ToString("yyyyMMdd");
            edi_40.CRETIM = DateTime.Now.ToString("HHmmss");
            officeWorksOrder.IDOC.EDI_DC40 = edi_40;
            PORDCH03IDOCE1PORDCH e1pordch = new PORDCH03IDOCE1PORDCH();
            PORDCH03IDOCE1PORDCHE1BPMEPOHEADER e1PoHeader = new PORDCH03IDOCE1PORDCHE1BPMEPOHEADER();
            e1pordch.SEGMENT = PORDCH03IDOCE1PORDCHSEGMENT.Item1;
            commercialInfo = shipment.CommercialInfo;
            string msg = string.Empty;
            foreach (CW.CommercialInfoCommercialInvoice civ in commercialInfo.CommercialInvoiceCollection)
            {
                try
                {
                    string poNumber = string.Empty;
                    if (civ.CustomizedFieldCollection != null)
                    {
                        foreach (CW.CustomizedField cf in civ.CustomizedFieldCollection)
                        {
                            if (cf.Key == "PO NUMBER")
                            {
                                if (string.IsNullOrEmpty(cf.Value))
                                {
                                    result.Processed = false;
                                    result.FileName = xmlFile;
                                    result.FolderLoc = Globals.glFailPath;
                                    msg = "File is unable to be sent as it is missing the Office works PO Number." + Environment.NewLine
                                        + "Commercial Invoice Number: " + civ.InvoiceNumber + Environment.NewLine
                                        + "Declartion Number: " + shipment.DataContext.DataSourceCollection[0].Key;

                                    MailModule.sendMsg(xmlFile, Globals.glAlertsTo, "No PO Number found in XML File. Invoice No: " + civ.InvoiceNumber, msg);
                                    MailModule.sendMsg(xmlFile, "andy.duggan@ctcorp.com.au", "No PO Number found in XML File. Invoice No: " + civ.InvoiceNumber, msg);
                                    NodeResources.MoveFile(xmlFile, Globals.glFailPath);
                                    return result;

                                }
                                else
                                {
                                    e1pordch.PURCHASEORDER = cf.Value;

                                    poNumber = cf.Value;
                                    e1PoHeader.PO_NUMBER = cf.Value;
                                }

                            }
                        }
                    }
                    else
                    {
                        result.Processed = false;
                        result.FileName = xmlFile;
                        result.FolderLoc = Globals.glFailPath;
                        msg = "File is unable to be sent.  Missing Customised Field Listing." + Environment.NewLine
                            + "Commercial Invoice Number: " + civ.InvoiceNumber + Environment.NewLine
                            + "Declartion Number: " + shipment.DataContext.DataSourceCollection[0].Key;

                        MailModule.sendMsg(xmlFile, Globals.glAlertsTo, "No PO Number found in XML File. Invoice No: " + civ.InvoiceNumber, msg);
                        NodeResources.MoveFile(xmlFile, Globals.glFailPath);
                        return result;
                    }

                    e1pordch.NO_MESSAGING = "X";
                    e1PoHeader.SEGMENT = PORDCH03IDOCE1PORDCHE1BPMEPOHEADERSEGMENT.Item1;
                    PORDCH03IDOCE1PORDCHE1BPMEPOHEADERX e1PoHeaderX = new PORDCH03IDOCE1PORDCHE1BPMEPOHEADERX();
                    e1PoHeaderX.SEGMENT = PORDCH03IDOCE1PORDCHE1BPMEPOHEADERXSEGMENT.Item1;
                    e1PoHeaderX.PO_NUMBER = "X";
                    e1pordch.E1BPMEPOHEADER = e1PoHeader;
                    e1pordch.E1BPMEPOHEADERX = e1PoHeaderX;
                    List<PORDCH03IDOCE1PORDCHE1BPMEPOITEM> poItemColl = new List<PORDCH03IDOCE1PORDCHE1BPMEPOITEM>();
                    List<PORDCH03IDOCE1PORDCHE1BPMEPOITEMX> poItemXColl = new List<PORDCH03IDOCE1PORDCHE1BPMEPOITEMX>();
                    List<PORDCH03IDOCE1PORDCHE1BPMEPOCOND> poCondColl = new List<PORDCH03IDOCE1PORDCHE1BPMEPOCOND>();
                    List<PORDCH03IDOCE1PORDCHE1BPMEPOCONDX> poCondXColl = new List<PORDCH03IDOCE1PORDCHE1BPMEPOCONDX>();

                    msg = string.Empty;
                    bool failInvoice = false;
                    foreach (CW.CommercialInfoCommercialInvoiceCommercialInvoiceLine civLine in civ.CommercialInvoiceLineCollection)
                    {
                        string itemNumber = string.Empty;
                        string entryNo = civLine.EntryNumber;
                        short entryLineNo = 0;
                        if (civLine.EntryLineNumber > 0)
                        {
                            entryLineNo = civLine.EntryLineNumber;
                        }

                        EntryHeader eh = new EntryHeader();
                        eh = GetEntryCollection(shipment.EntryHeaderCollection, entryNo);
                        List<EntryHeaderEntryLine> eLineColl = new List<EntryHeaderEntryLine>();
                        eLineColl = eh.EntryLineCollection.ToList();
                        List<EntryHeaderEntryLineEntryLineCharge> elChargeColl = new List<EntryHeaderEntryLineEntryLineCharge>();
                        elChargeColl = GetEntryLine(eLineColl, entryLineNo).ToList();
                        decimal dutyVal = 0;
                        foreach (EntryHeaderEntryLineEntryLineCharge eCharge in elChargeColl)
                        {
                            // Locate the Duty value for each line. 
                            switch (eCharge.Type.Code)
                            {
                                //Duty Charges
                                case "DTY": dutyVal += eCharge.Amount;
                                    break;
                                //Dumping Duty Charges
                                case "DMP": dutyVal += eCharge.Amount;
                                    break;
                            }                            
                        }
                        PORDCH03IDOCE1PORDCHE1BPMEPOITEM poItem = new PORDCH03IDOCE1PORDCHE1BPMEPOITEM();
                        if (civLine.CustomizedFieldCollection != null)
                        {
                            foreach (CustomizedField cf in civLine.CustomizedFieldCollection)
                            {
                                if (cf.Key == "Item Number")
                                {
                                    int i;
                                    if (int.TryParse(cf.Value, out i))
                                    {
                                        itemNumber = i.ToString();
                                    }
                                    break;
                                }
                            }
                        }


                        poItem.PO_ITEM = itemNumber;
                        poItem.QUANTITY = civLine.InvoiceQuantity.ToString();
                        poItem.PO_UNIT = civLine.InvoiceQuantityUnit.Code;
                        if (civLine.InvoiceQuantity == 0)
                        {
                            msg += "Invoice: " + civ.InvoiceNumber + " Line " + civLine.LineNo + " has 0 Qty. ItemNumber: " + itemNumber + ". This will fail" + Environment.NewLine;
                            failInvoice = true;
                        }
                        else
                        {
                            poItemColl.Add(poItem);
                        }

                        PORDCH03IDOCE1PORDCHE1BPMEPOITEMX poItemX = new PORDCH03IDOCE1PORDCHE1BPMEPOITEMX();
                        poItemX.SEGMENT = PORDCH03IDOCE1PORDCHE1BPMEPOITEMXSEGMENT.Item1;
                        poItemX.PO_ITEM = itemNumber;
                        poItemXColl.Add(poItemX);

                        PORDCH03IDOCE1PORDCHE1BPMEPOCOND poCond = new PORDCH03IDOCE1PORDCHE1BPMEPOCOND();
                        poCond.ITM_NUMBER = itemNumber;
                        poCond.COND_TYPE = "ZOB1";
                        poCond.COND_VALUE = dutyVal.ToString();
                        poCond.CURRENCY = "AUD";
                        poCond.CHANGE_ID = "U";
                        poCondColl.Add(poCond);
                        PORDCH03IDOCE1PORDCHE1BPMEPOCONDX poCondX = new PORDCH03IDOCE1PORDCHE1BPMEPOCONDX();
                        poCondX.SEGMENT = PORDCH03IDOCE1PORDCHE1BPMEPOCONDXSEGMENT.Item1;
                        poCondX.ITM_NUMBER = itemNumber;
                        poCondX.ITM_NUMBERX = "X";
                        poCondX.COND_TYPE = "X";
                        poCondX.COND_VALUE = "X";
                        poCondX.CURRENCY = "X";
                        poCondX.CHANGE_ID = "X";

                        poCondXColl.Add(poCondX);

                    }
                    if (failInvoice)
                    {
                        result.Processed = false;
                        result.FileName = xmlFile;
                        result.FolderLoc = Globals.glFailPath;

                        MailModule.sendMsg(xmlFile, Globals.glAlertsTo, "Error in Officeworks file. Zero qty's found. Invoice No: " + civ.InvoiceNumber, msg);
                        NodeResources.MoveFile(xmlFile, Globals.glFailPath);
                        return result;
                    }
                    e1pordch.E1BPMEPOITEM = poItemColl.ToArray();
                    e1pordch.E1BPMEPOITEMX = poItemXColl.ToArray();
                    e1pordch.E1BPMEPOCOND = poCondColl.ToArray();
                    e1pordch.E1BPMEPOCONDX = poCondXColl.ToArray();
                    officeWorksOrder.IDOC.E1PORDCH = e1pordch;
                    int iFileCount = int.Parse(NodeResources.GetEnum("OFWSERIAL", "SERIAL"));
                    Guid enumid = NodeResources.GetEnumID("OFWSERIAL", "SERIAL");
                    String officeWorksXML = Path.Combine(Globals.glCustomFilesPath, "OW_IN_CF_APLL_PORDCH_" + string.Format("{0:yyyyMMddhhmmss}_", DateTime.Now) + string.Format("{0:00000}", iFileCount) + ".xml");

                    while (File.Exists(officeWorksXML))
                    {
                        iFileCount++;
                        officeWorksXML = Path.Combine(Globals.glCustomFilesPath, "OW_IN_CF_APLL_PORDCH_" + string.Format("{0:yyyyMMddhhmmss}_", DateTime.Now) + string.Format("{0:00000}", iFileCount) + ".xml");

                    }
                    iFileCount++;
                    NodeResources.SetEnum("OFWSERIAL", "SERIAL", iFileCount.ToString(), enumid);
                    Stream outputFreightTracker = File.Open(officeWorksXML, FileMode.Create);
                    StringWriter writer = new StringWriter();
                    XmlSerializer xSer = new XmlSerializer(typeof(PORDCH03));
                    var officeWorksns = new XmlSerializerNamespaces();
                    officeWorksns.Add("", "");
                    xSer.Serialize(outputFreightTracker, officeWorksOrder, officeWorksns);
                    outputFreightTracker.Flush();
                    outputFreightTracker.Close();
                    string archive = Globals.ArchiveFile(Globals.glArcLocation, xmlFile);
                    TransReference tRef = new TransReference
                    {
                        Ref1Type = TransReference.RefType.Order,
                        Reference1 = poNumber,
                        Ref2Type = TransReference.RefType.Brokerage,
                        Reference2 = dec
                    };
                    NodeResources.AddTransaction(NodeResources.GetCustID(cwFile.Header.SenderID), Guid.Empty, xmlFile, "R", true, tRef, archive);
                    // Globals.ArchiveFile(Globals.glArcLocation, xmlFile);

                    try
                    {
                        if (tslMode.Text == "Production")
                        {
                            WinSCP.TransferOperationResult fResult = NodeResources.FtpSend(officeWorksXML, cust);
                            fResult.Check();
                            if (!fResult.IsSuccess)
                            {
                                result.FileName = NodeResources.MoveFile(xmlFile, Path.Combine(cust.C_path, "Failed"));
                                result.Processed = false;
                                result.FolderLoc = Path.GetDirectoryName(result.FileName);
                                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + fResult.Failures, Color.Red);


                            }
                            else
                            {
                                NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Officeworks File created (" + Path.GetFileName(officeWorksXML) + ") and sent. Ok.", Color.Green);
                                result.Processed = true;
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        string strEx = ex.GetType().Name;
                        strEx += ": " + ex.Message;
                        result.FileName = NodeResources.MoveFile(xmlFile, Path.Combine(cust.C_path, "Failed"));
                        result.Processed = false;
                        result.FolderLoc = Path.GetDirectoryName(result.FileName);
                        NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + strEx, Color.Red);

                    }
                }
                catch (Exception)
                {

                }


            }
            // result = Create315(xmlFile, cust);
            return result;

        }

        private EntryHeaderEntryLineEntryLineCharge[] GetEntryLine(List<EntryHeaderEntryLine> eh, short entryLineNo)
        {
            EntryHeaderEntryLineEntryLineCharge[] result = null;
            EntryHeaderEntryLine el = eh.Find(x => x.LineNumber == entryLineNo);
            if (el != null)
            {
                result = el.EntryLineChargeCollection;
            }

            return result;
        }

        private EntryHeader GetEntryCollection(EntryHeader[] entryHeaderCollection, string entryNo)
        {
            EntryHeader result = null;
            // EntryHeader eh = entryHeaderCollection.ToList().Find(x => x.EntryNumberCollection.ToList().Find(y => y.Number == entryNo));)
            foreach (EntryHeader e in entryHeaderCollection.ToList())
            {
                if (e.EntryNumberCollection != null)
                {
                    foreach (EntryHeaderEntryNumber eNo in e.EntryNumberCollection.ToList())
                    {
                        if (eNo.Number == entryNo)
                        {
                            result = e;
                        }
                    }
                }
                else
                {
                    result = entryHeaderCollection[0];
                }

            }
            return result;
        }

        private ProcessResult ProcessCargowiseFiles(string xmlfile)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            FileInfo processingFile = new FileInfo(xmlfile);
            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " File Found " + Path.GetFileName(xmlfile) + ". Checking for Cargowise File.");
            String ns = NodeResources.GetMessageNamespace(xmlfile);
            String appCode = NodeResources.GetApplicationCode(ns);

            String senderID = String.Empty;
            String recipientID = String.Empty;
            String reasonCode = String.Empty;
            ProcessResult canDelete = new ProcessResult();
            switch (appCode)
            {

                case "UDM":
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " File is Cargowise (UDM) XML File :" + xmlfile + ". " + "");
                    if (NodeResources.GetXMLType(xmlfile) == "UniversalEvent")
                    {

                    }
                    else
                    {
                        canDelete = ProcessUDM(xmlfile);
                    }
                    break;
                case "NDM":
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " File is Cargowise (NDM) XML File :" + xmlfile + ". " + "");
                    //  canDelete = ProcessProductResponse(xmlfile);
                    break;
                default:
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " File is not a valid Cargowise XML File :" + xmlfile + ". Checking Client Specific formats" + "");
                    canDelete = ProcessCustomXML(xmlfile);
                    break;
            }

            if (canDelete.Processed && !string.IsNullOrEmpty(canDelete.FileName))
            {
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();

                File.Delete(canDelete.FileName);
            }
            else
            {
                if (canDelete.FileName != null)
                {
                    string f = NodeResources.MoveFile(xmlfile, canDelete.FolderLoc);
                    if (f.StartsWith("Warning"))
                    {
                        NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                    }
                    else
                    { xmlfile = f; }
                }
            }
            return canDelete;
        }

        private void GetProcErrors()
        {
            SqlConnection sqlConn = new SqlConnection();
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            try
            {
                sqlConn.Open();
                SqlDataAdapter daProcErrors = new SqlDataAdapter("SELECT E_P, P_DESCRIPTION, E_SENDERID, E_RECIPIENTID, E_PROCDATE, E_FILENAME, E_ERRORDESC, E_ERRORCODE, " +
                                                                "E_PK, P_PATH FROM VW_ProcessngErrors ORDER BY E_PROCDATE", sqlConn);
                DataSet dsProcErrors = new DataSet();
                dgProcessingErrors.AutoGenerateColumns = false;
                daProcErrors.Fill(dsProcErrors, "PROCERRORS");
                dgProcessingErrors.DataSource = dsProcErrors;
                dgProcessingErrors.DataMember = "PROCERRORS";
                sqlConn.Close();
            }
            catch (Exception)
            {

            }
        }


        private string FixNumber(string bl)
        {
            string result;
            if (bl.Contains("/"))
            {
                result = bl.Replace("/", "-");
            }
            else
            {
                result = bl;
            }

            return result;
        }


        private ProcessResult ProcessCustomXML(string XMLFile)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            HeartBeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters());
            ProcessResult thisResult = new ProcessResult();
            thisResult.Processed = true;
            thisResult.FileName = XMLFile;
            SqlDataAdapter daXML = new SqlDataAdapter("SELECT C_CODE, C_ID, C_PATH, P_XSD, P_DESCRIPTION, P_ID, P_RECIPIENTID, P_SENDERID, P_SERVER, P_USERNAME, P_PASSWORD, P_LIBNAME, P_BILLTO,  P_PARAMLIST, P_METHOD, P_NOTIFYEMAIL from VW_CustomerProfile where P_XSD is not null and P_ACTIVE='Y'", sqlConn);
            DataSet dsXML = new DataSet();
            daXML.Fill(dsXML, "XMLFile");
            Boolean processed = false;
            FileInfo processingFile = new FileInfo(XMLFile);
            filesRx++;
            NodeResources.AddLabel(lblFilesRx, filesRx.ToString());
            totfilesRx++;
            NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
            TransReference trRef = new TransReference();
            String archiveFile;
            string recipientID = "";
            string senderID = "";
            string custMethod = string.Empty;
            string custParams = string.Empty;
            while (!processed)
            {
                //   FileStream fsFile = new FileStream(xmlFile, FileMode.Open, FileAccess.ReadWrite);
                //   StreamReader sr = new StreamReader(fsFile);
                foreach (DataRow dr in dsXML.Tables[0].Rows)
                {
                    try
                    {
                        string schemaFile = Path.Combine(Globals.glProfilePath, "Lib", dr["P_XSD"].ToString());
                        XmlTextReader sXsd = new XmlTextReader(schemaFile);
                        String ns = NodeResources.GetMessageNamespace(XMLFile);

                        XmlSchema schema = new XmlSchema();
                        string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
                        schema = XmlSchema.Read(sXsd, CustomValidateHandler.HandlerErrors);
                        XmlReaderSettings settings = new XmlReaderSettings();
                        settings.ValidationType = ValidationType.Schema;
                        settings.ValidationFlags = XmlSchemaValidationFlags.ReportValidationWarnings;
                        settings.Schemas.Add(schema);
                        settings.ValidationEventHandler += (o, ea) =>
                        {
                            throw new XmlSchemaValidationException(
                                string.Format("Schema Not Found: {0}",
                                              ea.Message),
                                ea.Exception);
                        };
                        using (var stream = new FileStream(XMLFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                        using (var cusXML = XmlReader.Create(stream, settings))
                        {
                            while (cusXML.Read())
                            {

                            }
                            stream.Close();
                            CustProfileRecord cust = GetCustomerProfile((Guid)dr["P_ID"]);
                            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Schema File Found (" + schemaFile + "). Continue processing Profile.");
                            stream.Dispose();
                            cusXML.Close();
                            cusXML.Dispose();
                            custMethod = dr["P_METHOD"].ToString();
                            custParams = dr["P_PARAMLIST"].ToString();
                            Type custType = this.GetType();
                            MethodInfo custMethodToRun = custType.GetMethod(custMethod);
                            var varResult = custMethodToRun.Invoke(this, new Object[] { XMLFile, cust });
                            if (varResult != null)
                            {
                                processed = true;
                                thisResult.Processed = true;

                            }

                        }
                        recipientID = dr["P_RECIPIENTID"].ToString();
                        senderID = dr["P_SENDERID"].ToString();
                        archiveFile = Globals.ArchiveFile(dr["C_PATH"].ToString() + "\\Archive", XMLFile);
                        processed = true;
                        thisResult.Processed = true;
                    }

                    catch (XmlSchemaValidationException ex)
                    {

                        //MailModule.sendMsg("", Globals.glAlertsTo, "XML Schema Validation issue", "Invalid schema definition error:" + ex.Message);
                        ProcessingErrors procerror = new ProcessingErrors();
                        CTCErrorCode error = new CTCErrorCode();
                        error.Code = NodeError.e111;
                        error.Description = "Customer XML Error :" + ex.Message;
                        error.Severity = "Warning";
                        procerror.ErrorCode = error;
                        procerror.Reference = custMethod;
                        procerror.RefType = "CustomXML";
                        procerror.SenderId = senderID;
                        procerror.RecipientId = recipientID;
                        procerror.FileName = XMLFile;
                        //string f = NodeResources.MoveFile(XMLFile, Globals.glFailPath);
                        //if (f.StartsWith("Warning"))
                        //{
                        //    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                        //}
                        //else
                        //{ XMLFile = f; }

                        thisResult.FolderLoc = Globals.glFailPath;
                        thisResult.Processed = true;
                        processed = true;
                        NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "Invalid schema definition error:" + ex.Message + ". " + "", System.Drawing.Color.Red);

                    }
                    catch (XmlException ex)
                    {

                        ProcessingErrors procerror = new ProcessingErrors();

                        CTCErrorCode error = new CTCErrorCode();
                        error.Code = NodeError.e111;
                        error.Description = "Customer XML Error :" + ex.Message;
                        error.Severity = "Warning";
                        procerror.ErrorCode = error;
                        procerror.SenderId = senderID;
                        procerror.RecipientId = recipientID;
                        procerror.FileName = XMLFile;
                        procerror.Reference = custMethod;
                        procerror.RefType = "CustomXML";
                        string f = NodeResources.MoveFile(XMLFile, Globals.glFailPath);
                        if (f.StartsWith("Warning"))
                        {
                            NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + f, System.Drawing.Color.Red);
                        }
                        else
                        { XMLFile = f; }

                        NodeResources.AddProcessingError(procerror);
                        thisResult.FolderLoc = Globals.glFailPath;
                        thisResult.Processed = true;
                        processed = true;
                        NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "CTC Node Exception Found: Custom Processing:" + ex.Message + ". " + "", System.Drawing.Color.Red);

                    }
                    catch (Exception ex)
                    {
                        ProcessingErrors procerror = new ProcessingErrors();
                        CTCErrorCode error = new CTCErrorCode();
                        error.Code = NodeError.e111;
                        error.Description = "Customer XML Error :" + ex.Message;
                        error.Severity = "Warning";
                        procerror.ErrorCode = error;
                        procerror.SenderId = senderID;
                        procerror.RecipientId = recipientID;
                        procerror.FileName = XMLFile;
                        procerror.Reference = custMethod;
                        procerror.RefType = "CustomXML";
                        NodeResources.AddProcessingError(procerror);
                        // File.Move(xmlFile, Path.Combine(Globals.glFailPath, Path.GetFileName(xmlFile)));
                        thisResult.FolderLoc = Globals.glFailPath;
                        thisResult.Processed = false;
                        processed = true;
                        string strEx = ex.GetType().Name;
                        NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "CTC Node Exception Found: " + strEx + ex.Message + ". " + "", System.Drawing.Color.Red);
                        //   return thisResult;
                    }
                }
                if (!processed)
                {
                    ProcessingErrors procerror = new ProcessingErrors();
                    CTCErrorCode error = new CTCErrorCode();
                    error.Code = NodeError.e111;
                    error.Description = "Unable to process " + Path.GetFileName(XMLFile) + "No matching routines found. Moving to Failed.";
                    error.Severity = "Warning";
                    procerror.ErrorCode = error;
                    procerror.SenderId = senderID;
                    procerror.RecipientId = recipientID;
                    procerror.FileName = XMLFile;
                    procerror.Reference = custMethod;
                    procerror.RefType = "CustomXML";
                    NodeResources.AddProcessingError(procerror);
                    thisResult.FolderLoc = Globals.glFailPath;
                    thisResult.Processed = false;
                    string f = NodeResources.MoveFile(XMLFile, Globals.glFailPath);
                    if (f.StartsWith("Warning"))
                    {
                        NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + f, System.Drawing.Color.Red);
                    }
                    else
                    { XMLFile = f; }

                    NodeResources.AddProcessingError(procerror);
                    thisResult.Processed = false;
                    thisResult.FolderLoc = Globals.glFailPath;
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Unable to process " + Path.GetFileName(XMLFile) + "No matching routines found. Moving to Failed." + "", System.Drawing.Color.Red);
                    processed = true;
                }

            }
            return thisResult;

        }



        private void customMappingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEnums Enums = new frmEnums();
            Enums.Show();

        }

        private void bbClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private CustProfileRecord GetCustomerProfile(Guid profileId)
        {
            CustProfileRecord result = new CustProfileRecord();

            SqlCommand sqlCustProfile = new SqlCommand("Select C_ID, P_ID, C_NAME, C_IS_ACTIVE, C_ON_HOLD, C_PATH, P_EVENTCODE, " +
                                                             "C_FTP_CLIENT, P_REASONCODE, P_SERVER, P_USERNAME, P_DELIVERY, P_METHOD, P_PARAMLIST," +
                                                             "P_PATH, P_PASSWORD, P_MSGTYPE, P_RECIPIENTID, P_SENDERID, P_EMAILADDRESS, P_SUBJECT," +
                                                             "P_DESCRIPTION, P_BILLTO, P_PORT, P_DIRECTION, C_CODE, P_DTS, P_ACTIVE, P_SSL " +
                                                             "from vw_CustomerProfile WHERE P_ID = @P_ID", sqlConn);

            sqlCustProfile.Parameters.AddWithValue("@P_ID", profileId);
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            sqlConn.ConnectionString = Globals.connString();
            sqlConn.Open();

            using (SqlDataReader drCustProfile = sqlCustProfile.ExecuteReader(CommandBehavior.CloseConnection))
            {
                if (drCustProfile.HasRows)
                {
                    while (drCustProfile.Read())
                    {
                        result = new CustProfileRecord();
                        result.C_id = (Guid)drCustProfile["C_ID"];
                        result.P_id = (Guid)drCustProfile["P_ID"];
                        result.C_name = drCustProfile["C_NAME"].ToString().Trim();
                        result.C_is_active = drCustProfile["C_IS_ACTIVE"].ToString().Trim();
                        result.C_on_hold = drCustProfile["C_ON_HOLD"].ToString().Trim();
                        result.C_path = drCustProfile["C_PATH"].ToString().Trim();
                        result.C_ftp_client = drCustProfile["C_FTP_CLIENT"].ToString().Trim();
                        result.P_reasoncode = drCustProfile["P_REASONCODE"].ToString().Trim();
                        result.P_EventCode = drCustProfile["P_EVENTCODE"].ToString().Trim();
                        result.P_server = drCustProfile["P_SERVER"].ToString().Trim();
                        result.P_username = drCustProfile["P_USERNAME"].ToString().Trim();
                        result.P_delivery = drCustProfile["P_DELIVERY"].ToString().Trim();
                        result.P_password = drCustProfile["P_PASSWORD"].ToString().Trim();
                        result.P_recipientid = drCustProfile["P_RECIPIENTID"].ToString().Trim();
                        result.P_Senderid = drCustProfile["P_SENDERID"].ToString().Trim();
                        result.P_description = drCustProfile["P_DESCRIPTION"].ToString().Trim();
                        result.P_path = drCustProfile["P_PATH"].ToString().Trim();
                        result.P_port = drCustProfile["P_PORT"].ToString().Trim();
                        result.C_code = drCustProfile["C_CODE"].ToString().Trim();
                        result.P_BillTo = drCustProfile["P_BILLTO"].ToString().Trim();
                        result.P_Direction = drCustProfile["P_DIRECTION"].ToString().Trim();
                        result.P_MsgType = drCustProfile["P_MSGTYPE"].ToString().Trim();
                        result.P_Subject = drCustProfile["P_SUBJECT"].ToString().Trim();
                        result.P_EmailAddress = drCustProfile["P_EMAILADDRESS"].ToString().Trim();
                        result.P_Method = drCustProfile["P_METHOD"].ToString().Trim();
                        result.P_Params = drCustProfile["P_PARAMLIST"].ToString().Trim();
                        if (drCustProfile["P_SSL"].ToString().Trim() == "Y")
                        {
                            result.P_ssl = true;
                        }
                        else
                        {
                            result.P_ssl = false;
                        }

                        if (drCustProfile["P_DTS"].ToString().Trim() == "Y")
                        {
                            result.P_DTS = true;
                        }
                        else
                        {
                            result.P_DTS = false;
                        }
                    }
                }
                drCustProfile.Close();
            }

            return result;
        }
        private CustProfileRecord GetCustomerProfile(string senderID, string recipientID, string reasonCode, string eventCode)
        {
            CustProfileRecord result = new CustProfileRecord();

            SqlCommand sqlCustProfile = new SqlCommand("Select C_ID, P_ID, C_NAME, C_IS_ACTIVE, C_ON_HOLD, C_PATH, P_EVENTCODE, " +
                                                             "C_FTP_CLIENT, P_REASONCODE, P_SERVER, P_USERNAME, P_DELIVERY, P_METHOD, P_PARAMLIST," +
                                                             "P_PATH, P_PASSWORD, P_MSGTYPE, P_RECIPIENTID, P_SENDERID, P_EMAILADDRESS, P_SUBJECT," +
                                                             "P_DESCRIPTION, P_BILLTO, P_PORT, P_DIRECTION, C_CODE, P_DTS, P_ACTIVE, P_SSL " +
                                                             "from vw_CustomerProfile WHERE P_SENDERID = @SENDERID and P_RECIPIENTID= @RECIPIENTID and P_ACTIVE='Y' " +
                                                             "Order by P_REASONCODE desc, P_EVENTCODE DESC ", sqlConn);

            sqlCustProfile.Parameters.AddWithValue("@SENDERID", senderID);
            sqlCustProfile.Parameters.AddWithValue("@RECIPIENTID", recipientID);

            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            sqlConn.ConnectionString = Globals.connString();
            sqlConn.Open();
            List<CustProfileRecord> custProfile = new List<CustProfileRecord>();
            using (SqlDataReader drCustProfile = sqlCustProfile.ExecuteReader(CommandBehavior.CloseConnection))
            {
                if (drCustProfile.HasRows)
                {
                    while (drCustProfile.Read())
                    {
                        result = new CustProfileRecord();
                        result.C_id = (Guid)drCustProfile["C_ID"];
                        result.P_id = (Guid)drCustProfile["P_ID"];
                        result.C_name = drCustProfile["C_NAME"].ToString().Trim();
                        result.C_is_active = drCustProfile["C_IS_ACTIVE"].ToString().Trim();
                        result.C_on_hold = drCustProfile["C_ON_HOLD"].ToString().Trim();
                        result.C_path = drCustProfile["C_PATH"].ToString().Trim();
                        result.C_ftp_client = drCustProfile["C_FTP_CLIENT"].ToString().Trim();
                        result.P_reasoncode = drCustProfile["P_REASONCODE"].ToString().Trim();
                        result.P_EventCode = drCustProfile["P_EVENTCODE"].ToString().Trim();
                        result.P_server = drCustProfile["P_SERVER"].ToString().Trim();
                        result.P_username = drCustProfile["P_USERNAME"].ToString().Trim();
                        result.P_delivery = drCustProfile["P_DELIVERY"].ToString().Trim();
                        result.P_password = drCustProfile["P_PASSWORD"].ToString().Trim();
                        result.P_recipientid = drCustProfile["P_RECIPIENTID"].ToString().Trim();
                        result.P_Senderid = drCustProfile["P_SENDERID"].ToString().Trim();
                        result.P_description = drCustProfile["P_DESCRIPTION"].ToString().Trim();
                        result.P_path = drCustProfile["P_PATH"].ToString().Trim();
                        result.P_port = drCustProfile["P_PORT"].ToString().Trim();
                        result.C_code = drCustProfile["C_CODE"].ToString().Trim();
                        result.P_BillTo = drCustProfile["P_BILLTO"].ToString().Trim();
                        result.P_Direction = drCustProfile["P_DIRECTION"].ToString().Trim();
                        result.P_MsgType = drCustProfile["P_MSGTYPE"].ToString().Trim();
                        result.P_Subject = drCustProfile["P_SUBJECT"].ToString().Trim();
                        result.P_EmailAddress = drCustProfile["P_EMAILADDRESS"].ToString().Trim();
                        result.P_Method = drCustProfile["P_METHOD"].ToString().Trim();
                        result.P_Params = drCustProfile["P_PARAMLIST"].ToString().Trim();
                        if (drCustProfile["P_SSL"].ToString().Trim() == "Y")
                        {
                            result.P_ssl = true;
                        }
                        else
                        {
                            result.P_ssl = false;
                        }

                        if (drCustProfile["P_DTS"].ToString().Trim() == "Y")
                        {
                            result.P_DTS = true;
                        }
                        else
                        {
                            result.P_DTS = false;
                        }
                        custProfile.Add(result);
                    }
                }
                else
                {
                    result = new CustProfileRecord();
                    SqlCommand execAdd = new SqlCommand();
                    execAdd.Connection = sqlConn;
                    execAdd.CommandText = "Add_Profile";
                    execAdd.CommandType = CommandType.StoredProcedure;
                    SqlParameter PC = execAdd.Parameters.Add("@P_C", SqlDbType.UniqueIdentifier);
                    PC.Value = getCustID(senderID);
                    SqlParameter PReasonCode = execAdd.Parameters.Add("@P_REASONCODE", SqlDbType.Char, 3);
                    PReasonCode.Value = reasonCode.ToUpper();
                    SqlParameter PEventCode = execAdd.Parameters.Add("@P_EVENTCODE", SqlDbType.Char, 3);
                    PEventCode.Value = eventCode.ToUpper();
                    SqlParameter PServer = execAdd.Parameters.Add("@P_SERVER", SqlDbType.VarChar, 50);
                    PServer.Value = "";
                    SqlParameter PUsername = execAdd.Parameters.Add("@P_USERNAME", SqlDbType.VarChar, 50);
                    PUsername.Value = "";
                    SqlParameter PPath = execAdd.Parameters.Add("@P_PATH", SqlDbType.VarChar, 100);
                    PPath.Value = "";
                    SqlParameter PPassword = execAdd.Parameters.Add("@P_PASSWORD", SqlDbType.VarChar, 50);
                    PPassword.Value = "";
                    string sDelivery = String.Empty;
                    sDelivery = "R";
                    SqlParameter pDelivery = execAdd.Parameters.Add("@P_DELIVERY", SqlDbType.Char, 1);
                    pDelivery.Value = sDelivery;
                    SqlParameter pMSGType = execAdd.Parameters.Add("@MSGTYPE", SqlDbType.VarChar, 20);
                    pMSGType.Value = "Original";
                    SqlParameter pBillType = execAdd.Parameters.Add("@BILLTO", SqlDbType.VarChar, 15);
                    pBillType.Value = senderID;
                    SqlParameter pChargeable = execAdd.Parameters.Add("@CHARGEABLE", SqlDbType.Char, 1);
                    pChargeable.Value = "Y";
                    SqlParameter PPort = execAdd.Parameters.Add("@P_PORT", SqlDbType.Char, 10);
                    PPort.Value = "";
                    SqlParameter PDescription = execAdd.Parameters.Add("@P_DESCRIPTION", SqlDbType.VarChar, 100);
                    PDescription.Value = "No Transport Profile found. Store in Folder";
                    SqlParameter PRecipientId = execAdd.Parameters.Add("@P_RECIPIENTID", SqlDbType.VarChar, 15);
                    PRecipientId.Value = recipientID;
                    SqlParameter PSenderID = execAdd.Parameters.Add("P_SENDERID", SqlDbType.VarChar, 15);
                    PSenderID.Value = senderID;
                    SqlParameter p_id = execAdd.Parameters.Add("@R_ID", SqlDbType.UniqueIdentifier);
                    SqlParameter PDirection = execAdd.Parameters.Add("@P_DIRECTION", SqlDbType.Char, 1);
                    PDirection.Value = "R";
                    p_id.Direction = ParameterDirection.Output;
                    SqlParameter r_Action = execAdd.Parameters.Add("@R_ACTION", SqlDbType.Char, 1);
                    r_Action.Direction = ParameterDirection.Output;
                    if (sqlConn.State == ConnectionState.Open)
                    {
                        sqlConn.Close();
                    }
                    sqlConn.Open();
                    execAdd.ExecuteNonQuery();
                    sqlConn.Close();
                    NodeResources.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Create New Profile Sender ID: " + senderID + " Recipient ID: " + recipientID + " Purpose Code: " + reasonCode + " Event Code:" + eventCode);

                }
                foreach (CustProfileRecord cp in custProfile)
                {
                    if (cp.P_reasoncode == reasonCode)
                    {
                        if (cp.P_EventCode == eventCode)
                        {
                            result = cp;
                            break;
                        }
                        else if (cp.P_EventCode == "*")
                        {
                            result = cp;
                            break;
                        }
                    }
                    else if (cp.P_reasoncode == "*")
                    {
                        if (cp.P_EventCode == eventCode)
                        {
                            result = cp;
                            break;
                        }
                        else if (cp.P_EventCode == "*")
                        {
                            result = cp;
                            break;
                        }
                    }

                }

            }
            return result;
        }


        private void frmMain_Resize(object sender, EventArgs e)
        {
            this.tslMain.Width = this.Width / 2;
            this.tslSpacer.Width = this.Width / 2 - (tslCmbMode.Width + productionToolStripMenuItem.Width);
        }

        private void productionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (productionToolStripMenuItem.Checked)
            {
                testingToolStripMenuItem.Checked = false;
                tslMode.Text = "Production";
            }
            else
            {
                testingToolStripMenuItem.Checked = true;
                tslMode.Text = "Testing";
            }
        }

        private void testingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (testingToolStripMenuItem.Checked)
            {
                productionToolStripMenuItem.Checked = false;
                tslMode.Text = "Testing";
            }
            else
            {
                productionToolStripMenuItem.Checked = true;
                tslMode.Text = "Production";
            }
        }

        private void eventTypesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEvents events = new frmEvents();
            events.Show();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            //  Create315();
        }

        private void resendFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmResendFile resendFile = new frmResendFile();
            resendFile.Show();

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void eAdapterTestToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}

