﻿namespace Interfreight_Satellite
{
    partial class frmEnums
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnClose = new System.Windows.Forms.Button();
            this.dgEnums = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.edCustomValue = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.edCargowiseValue = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbDataType = new System.Windows.Forms.ComboBox();
            this.bbNew = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.edFind = new System.Windows.Forms.TextBox();
            this.bbFind = new System.Windows.Forms.Button();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CW_MAPVALUE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CWValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmuEnum = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgEnums)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.cmuEnum.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(562, 416);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dgEnums
            // 
            this.dgEnums.AllowUserToAddRows = false;
            this.dgEnums.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgEnums.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgEnums.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Type,
            this.CW_MAPVALUE,
            this.CWValue});
            this.dgEnums.Location = new System.Drawing.Point(12, 50);
            this.dgEnums.Name = "dgEnums";
            this.dgEnums.ReadOnly = true;
            this.dgEnums.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgEnums.Size = new System.Drawing.Size(625, 266);
            this.dgEnums.TabIndex = 1;
            this.dgEnums.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgEnums_CellDoubleClick);
            this.dgEnums.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgEnums_UserDeletingRow);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Data Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(180, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Customer Value";
            // 
            // edCustomValue
            // 
            this.edCustomValue.Location = new System.Drawing.Point(183, 32);
            this.edCustomValue.Name = "edCustomValue";
            this.edCustomValue.Size = new System.Drawing.Size(167, 20);
            this.edCustomValue.TabIndex = 1;
            this.edCustomValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.edDatatype_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(387, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Cargowise Value";
            // 
            // edCargowiseValue
            // 
            this.edCargowiseValue.Location = new System.Drawing.Point(390, 31);
            this.edCargowiseValue.Name = "edCargowiseValue";
            this.edCargowiseValue.Size = new System.Drawing.Size(202, 20);
            this.edCargowiseValue.TabIndex = 2;
            this.edCargowiseValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.edDatatype_KeyPress);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Location = new System.Drawing.Point(548, 58);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "&Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.cmbDataType);
            this.groupBox1.Controls.Add(this.bbNew);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnAdd);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.edCargowiseValue);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.edCustomValue);
            this.groupBox1.Location = new System.Drawing.Point(10, 322);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(629, 87);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Mapping ";
            // 
            // cmbDataType
            // 
            this.cmbDataType.FormattingEnabled = true;
            this.cmbDataType.Items.AddRange(new object[] {
            "UOM",
            "CUST UOM",
            "ORGCODE"});
            this.cmbDataType.Location = new System.Drawing.Point(9, 31);
            this.cmbDataType.Name = "cmbDataType";
            this.cmbDataType.Size = new System.Drawing.Size(164, 21);
            this.cmbDataType.TabIndex = 7;
            // 
            // bbNew
            // 
            this.bbNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bbNew.Location = new System.Drawing.Point(467, 58);
            this.bbNew.Name = "bbNew";
            this.bbNew.Size = new System.Drawing.Size(75, 23);
            this.bbNew.TabIndex = 4;
            this.bbNew.Text = "&New";
            this.bbNew.UseVisualStyleBackColor = true;
            this.bbNew.Click += new System.EventHandler(this.bbNew_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Search for Item";
            // 
            // edFind
            // 
            this.edFind.Location = new System.Drawing.Point(105, 10);
            this.edFind.Name = "edFind";
            this.edFind.Size = new System.Drawing.Size(265, 20);
            this.edFind.TabIndex = 11;
            // 
            // bbFind
            // 
            this.bbFind.Location = new System.Drawing.Point(376, 8);
            this.bbFind.Name = "bbFind";
            this.bbFind.Size = new System.Drawing.Size(75, 23);
            this.bbFind.TabIndex = 12;
            this.bbFind.Text = "&Find";
            this.bbFind.UseVisualStyleBackColor = true;
            this.bbFind.Click += new System.EventHandler(this.bbFind_Click);
            // 
            // ID
            // 
            this.ID.DataPropertyName = "CW_ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            // 
            // Type
            // 
            this.Type.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Type.DataPropertyName = "CW_ENUMTYPE";
            this.Type.HeaderText = "Data Type";
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            // 
            // CW_MAPVALUE
            // 
            this.CW_MAPVALUE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CW_MAPVALUE.DataPropertyName = "CW_MAPVALUE";
            this.CW_MAPVALUE.HeaderText = "Custom Value";
            this.CW_MAPVALUE.Name = "CW_MAPVALUE";
            this.CW_MAPVALUE.ReadOnly = true;
            // 
            // CWValue
            // 
            this.CWValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CWValue.DataPropertyName = "CW_ENUM";
            this.CWValue.HeaderText = "Cargowise Value";
            this.CWValue.Name = "CWValue";
            this.CWValue.ReadOnly = true;
            // 
            // cmuEnum
            // 
            this.cmuEnum.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem});
            this.cmuEnum.Name = "cmuEnum";
            this.cmuEnum.Size = new System.Drawing.Size(108, 26);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            // 
            // frmEnums
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 451);
            this.Controls.Add(this.bbFind);
            this.Controls.Add(this.edFind);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgEnums);
            this.Controls.Add(this.btnClose);
            this.Name = "frmEnums";
            this.Text = "Custom Mapping List";
            this.Load += new System.EventHandler(this.frmEnums_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgEnums)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.cmuEnum.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView dgEnums;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox edCustomValue;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox edCargowiseValue;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button bbNew;
        private System.Windows.Forms.ComboBox cmbDataType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox edFind;
        private System.Windows.Forms.Button bbFind;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn CW_MAPVALUE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CWValue;
        private System.Windows.Forms.ContextMenuStrip cmuEnum;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
    }
}