﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Interfreight_Satellite
{
    public partial class frmEvents : Form
    {
        SqlConnection sqlConn;
        public frmEvents()
        {
            InitializeComponent();
        }

        private void bbAdd_Click(object sender, EventArgs e)
        {
            SqlCommand sqlEvent = new SqlCommand("AddEvent", sqlConn);
            sqlEvent.Parameters.AddWithValue("@V_EVENT", edEvent.Text);
            sqlEvent.Parameters.AddWithValue("@V_DESCRIPTION", edDescr.Text);
            sqlEvent.Parameters.AddWithValue("@V_CONTAINER", cbContainer.CheckState);
            sqlEvent.Parameters.AddWithValue("@V_SHIPMENT", cbShipment.CheckState);
            sqlEvent.Parameters.AddWithValue("@V_ORIGIN", cbOrigin.CheckState);
            sqlEvent.Parameters.AddWithValue("@V_DEST", cbDest.CheckState);
            string sType = string.Empty;
            SqlParameter v_id = sqlEvent.Parameters.Add("@V_ID", SqlDbType.UniqueIdentifier);
            v_id.Direction = ParameterDirection.Output;
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            sqlConn.Open();
            sqlEvent.CommandType = CommandType.StoredProcedure;
            sqlEvent.ExecuteNonQuery();

            LoadData();
            edDescr.Text = "";
            edEvent.Text = "";
            cbShipment.Checked = true;
            cbContainer.Checked = false;
            edEvent.Focus();

        }

        private void LoadData()
        {
            dgEvents.AutoGenerateColumns = false;
            SqlDataAdapter daEvent = new SqlDataAdapter("SELECT V_ID, V_EVENT, V_DESCRIPTION, V_SHIPMENT, V_DEST, V_ORIGIN,  V_CONTAINER from [EVENTS] ORDER by V_EVENT, V_SHIPMENT, V_CONTAINER", sqlConn);
            DataSet dsEvent = new DataSet();
            daEvent.Fill(dsEvent, "Events");
            dgEvents.DataMember = "Events";
            dgEvents.DataSource = dsEvent;


        }

        private void frmEvents_Load(object sender, EventArgs e)
        {
            sqlConn = new SqlConnection(Globals.connString());
            LoadData();

        }

        private void bbClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
