﻿using System;
using System.Globalization;

namespace Interfreight_Satellite
{
    class NodeExtensions
    {

    }
    public static class StringEx
    {
        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }
    }

    public static class DateTimeEx
    {
        public static DateTime ParseDateEMO(this DateTime value, string formatString)
        {
            string dateTimeStr = value.ToString(formatString);

            return DateTime.ParseExact(dateTimeStr, "yyyy-MM-ddTHH:mm:ss.ffzzz", CultureInfo.InvariantCulture);
        }
    }
}
