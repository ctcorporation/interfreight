﻿using System;
using System.Runtime.Serialization;

namespace Interfreight_Satellite
{
    [Serializable]
    internal class MissingElementException : Exception
    {
        public MissingElementException()
        {
        }

        public MissingElementException(string message) : base(message)
        {

        }

        public MissingElementException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected MissingElementException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}